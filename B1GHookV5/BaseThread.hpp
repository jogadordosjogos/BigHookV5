
#ifndef BASETHREAD_HPP
#define BASETHREAD_HPP
#include <pch.hpp>
class BaseThread
{
	std::vector<std::function<void()>> m_FunctionVector;
	
public:


	BaseThread() = default;
	BaseThread(const BaseThread &obj) = default;
	BaseThread &operator=(const BaseThread &obj) = default;
	BaseThread(BaseThread &&obj) = default;
	BaseThread &operator=(BaseThread &&obj) = default;
	virtual ~BaseThread() = default;
	/**
	 * \brief Initializes variables needed for thread.
	 */
	virtual void initialize() = 0;
	/**
	 * \brief Hooks the function needed for thread.
	 */
	virtual void hook() = 0;
	virtual std::vector<std::function<void()>> functionVector() { return m_FunctionVector; }
	/**
	 * \brief Executes a function from this thread.
	 * \param function FUnction to call.
	 */
	virtual void executeFunction(const std::function<void()> function) { m_FunctionVector.push_back(function); }
	/**
	 * \brief Clears functions in vector. Should be done at end of detour.
	 */
	virtual void __stdcall clearFunctions() { m_FunctionVector.clear(); }

	void setMainFiber(HANDLE mainFiber) { m_Fiber = mainFiber; }
	void setSecondFiber(HANDLE mainFiber) { m_FiberTwo = mainFiber; }
	HANDLE mainFiber() const { return m_Fiber; }
	HANDLE secondFiber() const { return m_FiberTwo; }
	
    virtual void initializeTick() = 0;
protected:
    /**
    * \brief Main Fiber.
    */
    void* m_Fiber;
    /**
    * \brief Script Fiber.
    */
    void* m_FiberTwo;
};
#endif // BASETHREAD_HPP
