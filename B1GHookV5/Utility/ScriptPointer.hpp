#ifndef SCRIPTPOINTER_HPP
#define SCRIPTPOINTER_HPP
#include "pch.hpp"

class ScriptPointer
{
private:
	void* m_Addr;
public:
	explicit ScriptPointer(void* addr);
	explicit ScriptPointer(std::uintptr_t addr);

	ScriptPointer at(std::uintptr_t index) const;
	ScriptPointer deref(std::uintptr_t index) const;

	template <typename T>
	std::enable_if_t<std::is_pointer_v<T>, T> as() const
	{
		return reinterpret_cast<T>(m_Addr);
	}

	template <typename T>
	std::enable_if_t<std::is_lvalue_reference_v<T>, T> as() const
	{
		return *reinterpret_cast<std::add_pointer_t<std::remove_reference_t<T>>>(m_Addr);
	}

	template <typename T>
	std::enable_if_t<std::is_same_v<T, std::uintptr_t>, T> as() const
	{
		return (T)(m_Addr);
	}
};

#endif // SCRIPTPOINTER_HPP
