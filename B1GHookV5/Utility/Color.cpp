#include "pch.hpp"
#include "Color.hpp"

IColor4::IColor4(const std::uint32_t fullParam):
	full(fullParam)
{
}

IColor4::IColor4(const std::uint8_t red, const std::uint8_t green, const std::uint8_t blue, const std::uint8_t alpha):
	r(red),
	g(green),
	b(blue),
	a(alpha)
{
}

IColor4::operator FColor4() const
{
	return {
		r / 255.f,
		g / 255.f,
		b / 255.f,
		a / 255.f
	};
}

FColor4::FColor4(const float red, const float green, const float blue, const float alpha):
	r(red),
	g(green),
	b(blue),
	a(alpha)
{
}

FColor4::operator IColor4() const
{
	return IColor4(
		static_cast<std::uint8_t>(r * 255.f),
		static_cast<std::uint8_t>(g * 255.f),
		static_cast<std::uint8_t>(b * 255.f),
		static_cast<std::uint8_t>(a * 255.f)
	);
}

IColor3::IColor3(const std::uint8_t red, const std::uint8_t green, const std::uint8_t blue):
	r(red),
	g(green),
	b(blue)
{
}

IColor3::operator FColor3() const
{
	return {
		r / 255.f,
		g / 255.f,
		b / 255.f
	};
}

FColor3::FColor3(float red, float green, float blue):
	r(red),
	g(green),
	b(blue)
{
}

FColor3::operator IColor3() const
{
	return IColor3(
		static_cast<std::uint8_t>(r * 255.f),
		static_cast<std::uint8_t>(g * 255.f),
		static_cast<std::uint8_t>(b * 255.f)
	);
}
