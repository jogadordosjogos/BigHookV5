
#ifndef COLOR_HPP
#define COLOR_HPP
#include "pch.hpp"

class IColor4;
class FColor4;
class IColor3;
class FColor3;

class IColor4
{
public:
	union
	{
		struct
		{
			std::uint8_t r;
			std::uint8_t g;
			std::uint8_t b;
			std::uint8_t a;
		};

		std::uint32_t full;
		std::uint8_t bytes[4];
	};

	IColor4() = default;
	explicit IColor4(std::uint32_t);
	IColor4(std::uint8_t, std::uint8_t, std::uint8_t, std::uint8_t);
	
	operator FColor4() const;
};

class FColor4
{
public:
	union
	{
		struct
		{
			float r;
			float g;
			float b;
			float a;
		};

		float data[4];
	};

	FColor4() = default;
	FColor4(float, float, float, float);

	explicit operator IColor4() const;
};

class IColor3
{
public:
	union
	{
		struct
		{
			std::uint8_t r;
			std::uint8_t g;
			std::uint8_t b;
		};

		std::uint8_t bytes[3];
	};

	IColor3() = default;
	IColor3(std::uint8_t, std::uint8_t, std::uint8_t);

	explicit operator FColor3() const;
};

class FColor3
{
public:
	union
	{
		struct
		{
			float r;
			float g;
			float b;
		};

		float data[3];
	};

	FColor3() = default;
	FColor3(float, float, float);

	explicit operator IColor3() const;
};

static_assert(sizeof(IColor4) == sizeof(std::uint32_t), "IColor4 size invalid");
static_assert(std::is_trivially_copyable_v<IColor4>, "IColor4 is not trivial");

static_assert(sizeof(FColor4) == sizeof(float) * 4, "FColor4 size invalid");
static_assert(std::is_trivially_copyable_v<FColor4>, "FColor4 is not trivial");

static_assert(sizeof(IColor3) == sizeof(std::uint8_t) * 3, "IColor3 size invalid");
static_assert(std::is_trivially_copyable_v<IColor3>, "IColor3 is not trivial");

static_assert(sizeof(FColor3) == sizeof(float) * 3, "FColor3 size invalid");
static_assert(std::is_trivially_copyable_v<FColor3>, "FColor3 is not trivial");
#endif // COLOR_HPP
