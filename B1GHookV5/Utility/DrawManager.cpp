#include "pch.hpp"
#include "DrawManager.hpp"
#include "BigVec2.hpp"
using namespace ClassPointers;

DrawManager::DrawManager()
{
}


DrawManager::~DrawManager()
{
}

void DrawManager::drawText(const char* text, const std::int64_t font, const CColor4 color, const BigVec2 pos, const BigVec2 scale, const std::uint16_t alignment, const BigVec2 wrap, const bool outline, const bool shadow)
{
	ClassPointers::g_ScriptThread->texxInfo()->setColor(color);
	ClassPointers::g_ScriptThread->texxInfo()->textScale.x = scale.x;
	ClassPointers::g_ScriptThread->texxInfo()->textScale.y = scale.y;
	ClassPointers::g_ScriptThread->texxInfo()->textWrap.x = wrap.x / 1920.f;
	ClassPointers::g_ScriptThread->texxInfo()->textWrap.y = wrap.y / 1920.f;
	ClassPointers::g_ScriptThread->texxInfo()->font = font;
	ClassPointers::g_ScriptThread->texxInfo()->alignment = alignment;
	ClassPointers::g_ScriptThread->texxInfo()->dropShadow = shadow;
	ClassPointers::g_ScriptThread->texxInfo()->textOutline = outline;
	ClassPointers::g_ScriptThread->beginText()("STRING");
	ClassPointers::g_ScriptThread->addTextComponent()(text);
	ClassPointers::g_ScriptThread->endTextComponent()(pos.x / 1920.f, pos.y / 1080.f);
    m_TextNumber++;
}

void DrawManager::drawRect(CColor4 color, const BigVec2 pos, const BigVec2 scale)
{
    g_ScriptThread->drawRect()(pos.x / 1920.f, pos.y / 1080.f, scale.x / 1920.f, scale.y / 1080.f, color.r, color.g, color.b, color.a);
    m_RectNumber++;
}

void DrawManager::drawLine(CColor4 color, const Vector3_t pos, const Vector3_t pos2)
{
    g_ScriptThread->drawLine()(pos.x, pos.y, pos.z, pos2.x, pos2.y, pos2.z, color.r, color.g, color.b, color.a);
    m_LineNumber++;
}

void DrawManager::drawPolygon(CColor4 color, const Vector3_t pos, const Vector3_t pos2, const Vector3_t pos3)
{
    g_ScriptThread->drawPoly()(pos.x, pos.y, pos.z, pos2.x, pos2.y, pos2.z, pos3.x, pos3.y, pos3.z, color.r, color.g, color.b, color.a);
    m_PolyNumber++;
}

void DrawManager::drawSprite(const char* dict, const char* tex, const BigVec2 pos, const BigVec2 size, const CColor4 color, const float rot)
{
    if (!g_ScriptThread->hasDictLoaded()(dict))
        g_ScriptThread->requestDict()(dict);
	else
		g_ScriptThread->drawSprite()(dict, tex, pos.x / 1920, pos.y / 1080, size.x / 1920, size.y / 1080, rot, color.r, color.g, color.b, color.a);
    m_SpriteNumber++;
}

void DrawManager::clearNums()
{
    m_TextNumber = 0;
    m_RectNumber = 0;
    m_PolyNumber = 0;
    m_LineNumber = 0;
    m_SpriteNumber = 0;
}
