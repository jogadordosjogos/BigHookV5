#ifndef SCRIPTBLIP_HPP
#define SCRIPTBLIP_HPP
#include "pch.hpp"

typedef std::int16_t tBlipRotation;
typedef std::uint8_t tBlipAlpha;

class ScriptBlip
{
	CBlip* m_Blip;
public:
	explicit ScriptBlip(CBlip* blip);

	Vector3_t getPosition() const;
	ScriptBlip& setPosition(Vector3_t position);

	float getScale() const;
	ScriptBlip& setScale(float scale);

	char* getName() const;
	ScriptBlip& setName(const char* name);

	bool isFocused() const;
	ScriptBlip& setFocused(bool focused);

	eBlipSprite getIcon() const;
	ScriptBlip& setIcon(eBlipSprite icon);

	tBlipRotation getRotation() const;
	ScriptBlip& setRotation(tBlipRotation rotation);

	tBlipAlpha getAlpha() const;
	ScriptBlip& setAlpha(tBlipAlpha alpha);

	IColor4 getColor() const;
	ScriptBlip& setColor(IColor4 color);
};

static_assert(sizeof(eBlipSprite) == sizeof(std::int32_t), "eBlipSprite size invalid");
#endif // SCRIPTBLIP_HPP
