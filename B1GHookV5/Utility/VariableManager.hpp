#pragma once
class VariableManager
{
	std::map<std::vector<std::string>, void*> m_VariableMap;
public:
	VariableManager();
	~VariableManager();


	template <typename T>
	void addVar(const std::vector<std::string>& identifier, T var, bool change2False = false)
	{
		m_VariableMap.insert(std::pair<std::vector<std::string>, void*>(identifier, reinterpret_cast<void*>(new T(var))));
        if (std::is_same<T, bool>() && change2False)
            *reinterpret_cast<bool*>(m_VariableMap[identifier]) = false;

	}
	template <typename T>
	void addVar(const std::vector<std::string>& identifier, T* var)
	{
		m_VariableMap.insert(std::pair<std::vector<std::string>, void*>(identifier, reinterpret_cast<void*>(var)));
	}

	template <typename T>
	T getVar(const std::vector<std::string>& identifier)
	{
		
		return reinterpret_cast<T>(m_VariableMap[identifier]);
	}
	void deleteVar(const std::vector<std::string>& identifier)
	{
		m_VariableMap.erase(identifier);
	}
	template <typename T>
	void setVar(const std::vector<std::string>& identifier, T val)
	{
		*reinterpret_cast<T*>(m_VariableMap[identifier]) = val;
	}
};

