#pragma once
#include "NotificationBuilder.hpp"

template<typename Derived, typename Base>
std::unique_ptr<Derived>
 dynamicUniquePtrCast(std::unique_ptr<Base>& p)
{
	if (Derived *result = dynamic_cast<Derived *>(p.get())) {
		p.release();
		return std::unique_ptr<Derived>(result);
	}
	return std::unique_ptr<Derived>(nullptr);
}
class BuilderManager
{
	std::map<std::string, std::unique_ptr<NotificationBuilder>> m_BuilderMap;
public:
	BuilderManager()
		= default;
	template <typename T>
	std::unique_ptr<T> newBuilder(const std::string& name)
	{
		auto builder = std::make_unique<T>();

		m_BuilderMap[name] = std::move(builder);
		auto& builder2 = m_BuilderMap[name];

		std::unique_ptr<T> retVal = dynamicUniquePtrCast<T, NotificationBuilder>(builder2);
		return retVal;

	}
	template <typename T>
	std::unique_ptr<T> getBuilder(const std::string& name)
	{
		auto& builder = m_BuilderMap[name];
		std::unique_ptr<T> retVal = dynamicUniquePtrCast<T, NotificationBuilder>(builder);
		return retVal;
	}
    void removeBuilder(const std::string& name)
	{
        auto& builder = m_BuilderMap[name];
        builder = nullptr;
	}
};