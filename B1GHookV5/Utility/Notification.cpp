#include <pch.hpp>
#include "Notification.hpp"
#define TO_CHAR( s ) ClassPointers::g_Util->strChar(s)
void Notification::setDict(const std::string& dict)
{
	m_IconDict = dict;
}
void Notification::setTexture(const std::string& texture)
{
	m_IconTex = texture;
}

void Notification::setTitle(const std::string& title)
{
	m_Title = title;
}

void Notification::setSubTItle(const std::string& sub)
{
	m_SubTitle = sub;
}

void Notification::setClanTag(const std::string& tag)
{
	m_ClanTag = tag;
}

void Notification::setDescription(const std::string& desc)
{
	m_Description = desc;
}

void Notification::setType(const eNotificationType type)
{
	m_Type = type;
}

void Notification::open() const
{
	switch (m_Type)
	{
	case eNotificationType::eRegular:
		UI::_SET_NOTIFICATION_TEXT_ENTRY(const_cast<char*>("STRING")); 
		UI::ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME(const_cast<char*>(m_Description.c_str()));
		UI::_DRAW_NOTIFICATION(0, 0);
		break;
	case eNotificationType::eClan1:
		UI::_SET_NOTIFICATION_TEXT_ENTRY(const_cast<char*>("STRING"));
		UI::ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME(TO_CHAR(m_Description));
		UI::_SET_NOTIFICATION_MESSAGE_CLAN_TAG_2(TO_CHAR(m_IconDict), TO_CHAR(m_IconTex), 1, 7, TO_CHAR(m_Title),
		                                         TO_CHAR(m_SubTitle), 1, TO_CHAR("___" + m_ClanTag), 8, 1);
		UI::_DRAW_NOTIFICATION(0, 0);
		
		break;
	default:
		break;
	}
}


