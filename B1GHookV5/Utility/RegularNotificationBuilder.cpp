#include "pch.hpp"
#include "RegularNotificationBuilder.hpp"
#include "NotificationBuilder.hpp"

RegularNotificationBuilder* RegularNotificationBuilder::createNotification()
{
	std::cout << "Creating Notification\n";
	m_Notification = std::make_unique<Notification>();
	return this;
}

RegularNotificationBuilder* RegularNotificationBuilder::addDescription(const std::string& desc)
{
	m_Notification->setDescription(desc);
	return this;
}

RegularNotificationBuilder* RegularNotificationBuilder::addType()
{
	m_Notification->setType(eNotificationType::eRegular);
	return this;
}

RegularNotificationBuilder* RegularNotificationBuilder::addDict(const std::string&)
{
	m_Notification->setDict(nullptr);
	return this;
}

RegularNotificationBuilder* RegularNotificationBuilder::addTex(const std::string&)
{
	m_Notification->setTexture(nullptr);
	return this;
}

RegularNotificationBuilder* RegularNotificationBuilder::addTitle(const std::string&)
{
	m_Notification->setTitle(nullptr);
	return this;
}

RegularNotificationBuilder* RegularNotificationBuilder::addSubtitle(const std::string&)
{
	m_Notification->setSubTItle(nullptr);
	return this;
}

RegularNotificationBuilder* RegularNotificationBuilder::addClanTag(const std::string&)
{
	m_Notification->setClanTag(nullptr);
	return this;
}
