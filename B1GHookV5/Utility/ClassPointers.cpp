#pragma once
#include "pch.hpp"
#include "Rage/Invoking/HashMap.hpp"
#include "Log.hpp"
#include "Hooking/ScriptThread.hpp"
#include "Util.hpp"
#include "Hooking/DirectX/DirectThread.hpp"
#include "Script/DirectScript.hpp"
#include "Script/NativeScript.hpp"


namespace ClassPointers
{
	std::unique_ptr<DirectScript> g_DirectScript = std::make_unique<DirectScript>();
	std::unique_ptr<NativeScript> g_NativeScript = std::make_unique<NativeScript>();
	std::unique_ptr<HookManager> g_Hooker = std::make_unique<HookManager>();
	std::unique_ptr<NativeInvoker> g_NativeInvoker = std::make_unique<NativeInvoker>();
	std::unique_ptr<ScriptThread> g_ScriptThread = std::make_unique<ScriptThread>();
	std::unique_ptr<Logger> g_Logger = std::make_unique<Logger>();
	std::unique_ptr<Utility> g_Util = std::make_unique<Utility>();
	std::unique_ptr<DirectThread> g_DirectThread = std::make_unique<DirectThread>();
	std::unique_ptr<TimerManager> g_Timers = std::make_unique<TimerManager>();
	std::unique_ptr<YTDLoader> g_YtdLoader = std::make_unique<YTDLoader>();
	std::unique_ptr<BuilderManager> g_NotificationManager = std::make_unique<BuilderManager>();
	std::unique_ptr<VariableManager> g_VarManager = std::make_unique<VariableManager>();
    std::unique_ptr<WindowManager> g_WindowManager = std::make_unique<WindowManager>();
    std::unique_ptr<DrawManager> g_DrawManager = std::make_unique<DrawManager>();
    std::unique_ptr<MenuHandler> g_MenuManager = std::make_unique<MenuHandler>();
    std::unique_ptr<KeyManager> g_KeyManager = std::make_unique<KeyManager>();
    std::unique_ptr<GifManager> g_GifManager = std::make_unique<GifManager>();
}
