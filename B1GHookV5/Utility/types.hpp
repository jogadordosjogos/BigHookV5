
#ifndef TYPES_HPP
#define TYPES_HPP

#include "BigVec2.hpp"

struct CPed;
struct Vector4_t;
using Void         = std::uint32_t;
using Any          = std::uint32_t;
using uint         = std::uint32_t;
using Hash         = std::uint32_t;
using Entity       = std::int32_t;
using Player       = std::int32_t;
using FireId       = std::int32_t;
using Ped          = std::int32_t;
using Vehicle      = std::int32_t;
using Cam          = std::int32_t;
using CarGenerator = std::int32_t;
using Group        = std::int32_t;
using Train        = std::int32_t;
using Pickup       = std::int32_t;
using Object       = std::int32_t;
using Weapon       = std::int32_t;
using Interior     = std::int32_t;
using Blip         = std::int32_t;
using Texture      = std::int32_t;
using TextureDict  = std::int32_t;
using CoverPoint   = std::int32_t;
using Camera       = std::int32_t;
using TaskSequence = std::int32_t;
using ColourIndex  = std::int32_t;
using Sphere       = std::int32_t;
using ScrHandle    = std::int32_t;

struct Vector3;
struct Vector3_t;

typedef BOOL(*isEntityUpsideDown)(Entity);
#pragma pack(push, 1)
struct NativeVector3
{
	 alignas(std::uintptr_t) float x;
	 alignas(std::uintptr_t) float y;
	 alignas(std::uintptr_t) float z;
	// ReSharper disable once CppInconsistentNaming

	 operator Vector3_t();
};
#pragma pack(pop)

#pragma pack(push, 1)
struct Vector3_t
{
	float x;
	float y;
	float z;
	// ReSharper disable once CppInconsistentNaming

	operator NativeVector3();
};
#pragma pack(pop)
#pragma pack(push, 1)
struct NativeVector4
{
    alignas(std::uintptr_t) float x;
    alignas(std::uintptr_t) float y;
    alignas(std::uintptr_t) float z;
    alignas(std::uintptr_t) float w;
    // ReSharper disable once CppInconsistentNaming

    operator Vector4_t();
};
#pragma pack(pop)

#pragma pack(push, 1)
struct Vector4_t
{
    float x;
    float y;
    float z;
    float w;
    // ReSharper disable once CppInconsistentNaming

    operator NativeVector4();
};
#pragma pack(pop)
struct CBlip
{
public:
	std::int32_t iID; //0x0000 
	std::int8_t iID2; //0x0004 
	char _0x0005[3];
	std::uint8_t N000010FB; //0x0008 (80 = moves with player, some values will turn icon into map cursor and break it)
	char _0x0009[7];
	Vector3_t coords;
	char _0x001C[6];
	std::uint8_t bFocused; //0x0022   (Focused? 0100 0000)
	char _0x0023[5];
	char *szMessage; //0x0028 If not null, contains the string of whatever the blip says when selected.
	char _0x0030[16];
	std::int32_t iIcon; //0x0040
	std::uint32_t _0x0044[4];
	std::uint32_t dwColor; //0x0048 (Sometimes works?)
	char _0x004C[4];
	float fScale; //0x0050 
	std::int16_t iRotation; //0x0054 Heading
	std::uint8_t bInfoIDType; //0x0056 GET_BLIP_INFO_ID_TYPE
	std::uint8_t bZIndex; //0x0057 
	std::uint8_t bDisplay; //0x0058  Also Visibility 0010
	std::uint8_t bAlpha; //0x0059
};//Size=0x005A

#pragma pack(push, 1)
class CColor4
{
public:
	union
	{
		struct
		{
			std::uint8_t r, g, b, a;
		};
		std::uint32_t data;
	};
};
#pragma pack(pop)

#pragma pack(push, 1)
class CResolution
{
public:
	std::uint32_t width;
	std::uint32_t height;
};
#pragma pack(pop)

#pragma pack(push, 1)
class CDateTime
{
public:
	std::uint32_t day;
	std::uint32_t month;
	std::uint32_t year;
	std::uint32_t hour;
	std::uint32_t minute;
	std::uint32_t second;
};
#pragma pack(pop)

#pragma pack(push, 1)
class CTextInfo
{
public:
	CColor4	color;
	BigVec2 textScale;
	BigVec2 textWrap;
	std::int64_t font;
	std::int16_t alignment;
	bool dropShadow;
	bool textOutline;

	void setColor(CColor4 c)
	{
		color = c;
		std::swap(color.r, color.b);
	}
};
#pragma pack(pop)
#pragma pack(push, 1)

struct CWantedData
{
    char pad_0x0000[0x50]; //0x0000
    Vector3_t radius1; //0x0050
    char pad_0x005C[0x4]; //0x005C
    Vector3_t radius2; //0x0060
    char pad_0x006C[0x4]; //0x006C
    Vector3_t radius3; //0x0070
    char pad_0x007C[0x6]; //0x007C
    std::uint8_t ignore; //0x0082
    char pad_0x0083[0x31]; //0x0083
    std::int32_t fakeWantedLevel; //0x00B4
    std::int32_t wantedLevel; //0x00B8
    char pad_0x00BC[0x4C]; //0x00BC
};

#pragma pack(pop)

#pragma pack(push, 1)
struct CPlayerInfo
{
    char pad_0x0000[0x34]; //0x0000
    void* internalIp; //0x0034 
    std::uint16_t internalPort; //0x0038 
    char pad_0x003A[0x2]; //0x003A
    void* relayIp; //0x003C 
    std::uint16_t relayPort; //0x0040 
    char pad_0x0042[0x2]; //0x0042
    void* externalIp; //0x0044 
    std::uint16_t externalPort; //0x0048 
    char pad_0x004A[0x32]; //0x004A
    char name[20]; //0x7B32E9C0 
    char pad_0x0090[0xB4]; //0x0090
    float fVehicleAirDragMultiplier; //0x0144 
    float fSwimSpeed; //0x0148 
    float fRunSpeed; //0x014C 
    char pad_0x0150[0x18]; //0x0150
    float fCrouchSpeed; //0x0168 
    char pad_0x016C[0x4C]; //0x016C
    std::int32_t iState; //0x01B8  6: cutscene
    char pad_0x01BC[0xC]; //0x01BC
    CPed* pPed; //0x01C8 
    char pad_0x01D0[0x28]; //0x01D0
    std::uint32_t dwFrameFlags; //0x01F8 
    char pad_0x01FC[0x7C]; //0x01FC
    float fTimeUnderwater; //0x0278 
    char pad_0x027C[0x4]; //0x027C
    void* N0000069E; //0x0280 
    void* N0000069F; //0x0288 
    float fAimingTime; //0x0290 
    std::uint8_t btIsAiming; //0x0294 0x2 = aiming; 0x3 = not aiming
    char pad_0x0295[0xB]; //0x0295
    float N000006A2; //0x02A0 
    float N000017B1; //0x02A4 
    char pad_0x02A8[0x18]; //0x02A8
    Vector4_t N000006A6; //0x02C0 
    Vector4_t N000006A8; //0x02D0 
    Vector4_t v4AimingPos; //0x02E0 
    char pad_0x02F0[0x18]; //0x02F0
    float fLockonRangeOverride; //0x0308 
    char pad_0x030C[0x74]; //0x030C
    void* N000006BE; //0x0380 
    char pad_0x0388[0x28]; //0x0388
    Vector4_t N000006C4; //0x03B0 
    Vector4_t N000006C6; //0x03C0 
    char pad_0x03D0[0x18]; //0x03D0
    void* N000006CB; //0x03E8 
    char pad_0x03F0[0x8]; //0x03F0
    void* N000006CD; //0x03F8 
    char pad_0x0400[0x8]; //0x0400
    char* N000006CF; //0x0408 
    void* N000006D0; //0x0410 
    char pad_0x0418[0x30]; //0x0418
    float N000006D7; //0x0448 
    float N000017AB; //0x044C 
    float N000006D8; //0x0450 
    float N00001638; //0x0454 
    void* N000006D9; //0x0458 
    char pad_0x0460[0x1C0]; //0x0460
    Vector3_t N00000712; //0x0620 
    char pad_0x062C[0x134]; //0x062C
    CWantedData wantedData; //0x0760 
    char pad_0x0868[0x3F8]; //0x0868
    float fSprintStaminaTimeUsed; //0x0C60 
    float fSprintStaminaTimeMax; //0x0C64 
    char pad_0x0C68[0x18]; //0x0C68
    float fStamina; //0x0C80 
    float fStaminaMax; //0x0C84 
    char pad_0x0C88[0x10]; //0x0C88
    std::int32_t iGroup; //0x0C98 
    char pad_0x0C9C[0xC]; //0x0C9C
    CColor4 cParachuteSmokeColor; //0x0CA8 
    char pad_0x0CAC[0x20]; //0x0CAC
    float fHealthRegenRate; //0x0CCC 
    char pad_0x0CD0[0x104]; //0x0CD0
    float fNoiseMultiplier; //0x0DD4 
    char pad_0x0DD8[0x59]; //0x0DD8
    std::uint8_t btCanChuteLeaveTrail; //0x0E31  | |= (& 0xF7) << 3 
    char pad_0x0E32[0x1]; //0x0E32
    std::uint8_t btCanUseCover; //0x0E33 
    char pad_0x0E34[0x18]; //0x0E34
    float fStealthPerceptionMultiplier; //0x0E4C 
    char pad_0x0E50[0xACC]; //0x0E50
};
#pragma pack(pop)
#pragma pack(push, 1)
struct CPed
{
    virtual ~CPed() = 0;
    char pad_0x0000[0x18]; //0x0008
    void* pModelInfo; //0x0020 
    char pad_0x0028[0x4]; //0x0028
    std::uint8_t btInvisibility; //0x002C 
    char pad_0x002D[0x3]; //0x002D
    void* pNavigation; //0x0030 
    char pad_0x0038[0x10]; //0x0038
    void* pPedStyle; //0x0048 
    void* N00000019; //0x0050 
    char pad_0x0058[0x8]; //0x0058
    void* matrix; //0x0060 
    char pad_0x00A0[0x30]; //0x00A0
    void* netObj; //0x00D0 
    char pad_0x00D8[0x34]; //0x00D8
    Vector3_t v3PosAgain; //0x010C 
    char pad_0x0118[0x4]; //0x0118
    Vector3_t v3PosOnceAgain; //0x011C 
    char pad_0x0128[0x61]; //0x0128
    std::uint8_t btGod; //0x0189 
    char pad_0x018A[0xF6]; //0x018A
    float fHealth; //0x0280 
    char pad_0x0284[0x1C]; //0x0284
    float fHealthMax; //0x02A0 
    char pad_0x02A4[0x2DC]; //0x02A4
    Vector3_t v3Velocity; //0x0580 
    char pad_0x058C[0x2EC]; //0x058C
    std::int64_t iAge; //0x0878 
    char pad_0x0880[0x4A8]; //0x0880
    void* pLastVehicle; //0x0D28 
    char pad_0x0D30[0x370]; //0x0D30
    std::uint8_t btUnderwater; //0x10A0  2 = not underwater
    char pad_0x10A1[0x7]; //0x10A1
    std::int32_t N00000224; //0x10A8 
    char pad_0x10AC[0x4]; //0x10AC
    void* pedKek; //0x10B0 
    CPlayerInfo* playerInfo; //0x10B8 
    void* pedInventory; //0x10C0 
    void* pWeaponManager; //0x10C8 
    char pad_0x10D0[0x24]; //0x10D0
    void* pedAudio; //0x10F4 
    char pad_0x10FC[0x4]; //0x10FC
    void* N0000022F; //0x1100 
    char pad_0x1108[0x1F8]; //0x1108
    float fWetness; //0x1300 
    char pad_0x1304[0x78]; //0x1304
    float fWantedRadius; //0x137C 
    float N0000027F; //0x1380 
    char pad_0x1384[0x68]; //0x1384
    std::uint8_t btSeatbelt; //0x13EC 
    char pad_0x13EE[0x6]; //0x13EE
    std::uint8_t btCanBeDraggedOut; //0x13F4 
    std::uint8_t iFlagTings; //0x13F8  & (1 << 30) -> in flying vehicle
    char pad_0x13FA[0x7]; //0x13FA
    std::uint8_t N00001538; //0x1401 
    char pad_0x1402[0x2]; //0x1402
    std::uint8_t N00001535; //0x1404 
    char pad_0x1405[0x1A]; //0x1405
    std::int32_t iParachuteFlags; //0x1420 1 << 10 = has reverse chute
    char pad_0x1424[0x28]; //0x1424
    std::int32_t isShooting; //0x144C 
    char pad_0x1450[0x17]; //0x1450
    std::uint8_t btIsInVehicle; //0x1467 
    char pad_0x1468[0x50]; //0x1468
    float fArmor; //0x14B8 
    float N00000E4E; //0x14BC 
    char pad_0x14C0[0x18]; //0x14C0
    float fArmor2; //0x14D8 
    float fFatiguedHealthThreshold; //0x14DC 
    float fInjuredHealthThreshold; //0x14E0 
    float fDyingHealthThreshold; //0x14E4 
    float fHurtHealthThreshold; //0x14E8 
    char pad_0x14EC[0x10]; //0x14EC
    float fWantedRadiusV5; //0x14FC 
    char pad_0x1500[0xB0]; //0x1500
    void* N000002C5; //0x15B0 
    char pad_0x15B8[0x24]; //0x15B8
    std::uint16_t wMoney; //0x15DC 
    char pad_0x15DE[0xA]; //0x15DE
    float fTimeUnderwater; //0x15E8 
    char pad_0x15EC[0x346]; //0x15EC
    std::int8_t iFlagPreferRearSeats; //0x1932 
    char pad_0x1933[0x6F5]; //0x1933
};

#pragma pack(pop)

#pragma pack(push, 1)
struct CPedFactory
{
	virtual ~CPedFactory() = 0;
	CPed* localPlayer;
};
#pragma pack(pop)

using CHudColor = CColor4;

inline NativeVector3::operator Vector3_t()
{
	return Vector3_t{ x, y, z };
}

inline Vector3_t::operator NativeVector3()
{
	return NativeVector3{ x, y, z };
}

static_assert(sizeof(bool)      == sizeof(std::uint8_t), "bool size invalid");
static_assert(sizeof(CColor4)   == sizeof(std::uint8_t) * 4, "CColor4 size invalid");
static_assert(sizeof(Vector3_t) == sizeof(float) * 3, "Vector3_t size invalid");
static_assert(sizeof(CTextInfo) == sizeof(std::int32_t) + sizeof(BigVec2) * 2 + sizeof(std::int64_t) + sizeof(std::int16_t) + sizeof(bool) * 2, "CTextInfo size invalid");
#endif // TYPES_HPP
