#include "pch.hpp"
#include <filesystem>
#include <cstdarg>

template<std::size_t bufferSize>
std::size_t getTime(char (&out)[bufferSize])
{
	auto epoch = std::time(nullptr);
	return std::strftime(out, bufferSize - 1, "[%F %H:%M:%S]", std::localtime(&epoch));
}

std::unique_ptr<char[]> Logger::log(const char* const prefix, const char* const format, std::va_list args)
{
	char timeBuf[32] = {};
	getTime(timeBuf);

	auto len = std::vsnprintf(nullptr, NULL, format, args) + 1;
	auto buf = std::make_unique<char[]>(len);
	std::vsnprintf(buf.get(), len, format, args);

	if (m_FileStream)
	{
		*m_FileStream << timeBuf << ' ' << prefix << ' ' << buf.get() << std::endl;
	}

	if (m_Console)
	{
		*m_Console << timeBuf << ' ' << prefix << ' ' << buf.get() << std::endl;
	}

    return std::move(buf);
}

bool Logger::initialize(const char* dirName)
{
	AllocConsole();
	SetConsoleTextAttribute(GetStdHandle(STD_OUTPUT_HANDLE), FOREGROUND_GREEN);
	SetConsoleTitleA(dirName);
	m_Console = std::make_unique<std::ofstream>("CONOUT$");

	RawStringStream logFile(m_LogFile);
	logFile << std::getenv("appdata") << dirName;
	std::filesystem::create_directory(m_LogFile);
	logFile << '\\' << dirName << ".log";

	m_FileStream = std::make_unique<std::ofstream>(m_LogFile, std::ios::out | std::ios::app);

	log("[Start]", "Log Initialzed", {});
	return true;
}

void Logger::uninitialize()
{
	m_Console->close();
	FreeConsole();

	m_FileStream->close();
}

void Logger::msg(const char* format, ...)
{
	std::va_list args;
	va_start(args, format);
	m_Logs.push_back(log("[Message]", format, args));
	va_end(args);
}

void Logger::error(const char* format, ...)
{
	std::va_list args;
	va_start(args, format);
	m_Logs.push_back(log("[Error]", format, args));
	va_end(args);
}

void Logger::fatal(const char* format, ...)
{
	std::va_list args;
	va_start(args, format);
	m_Logs.push_back(log("[Fatal]", format, args));
	va_end(args);
}
