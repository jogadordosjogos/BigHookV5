#include "pch.hpp"
#include "ScriptPointer.hpp"

ScriptPointer::ScriptPointer(void* const addr):
	m_Addr(addr)
{
}

ScriptPointer::ScriptPointer(const std::uintptr_t addr):
	m_Addr(reinterpret_cast<void*>(addr))
{
}

ScriptPointer ScriptPointer::at(const std::uintptr_t index) const
{
	return m_Addr ? ScriptPointer((std::uintptr_t) m_Addr + index) : *this;
}

ScriptPointer ScriptPointer::deref(const std::uintptr_t index) const
{
	return m_Addr ? ScriptPointer(at(index).as<std::uintptr_t&>()) : *this;
}
