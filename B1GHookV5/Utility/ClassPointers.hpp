
#ifndef CLASSPOINTERS_HPP
#define CLASSPOINTERS_HPP
#include "pch.hpp"
#include "Script/NativeScript.hpp"
#include "Rage/Invoking/HashMap.hpp"
#include "Log.hpp"
#include "Hooking/ScriptThread.hpp"

#include "Util.hpp"
#include "Hooking/DirectX/DirectThread.hpp"
#include "Script/DirectScript.hpp"
#include "Hooking/HookManager.hpp"
#include "TimerManager.hpp"
#include "YTDLoader.h"
#include "NotifyBuilderManager.hpp"
#include "VariableManager.hpp"
#include "DirectX/WindowManager.hpp"
#include "DrawManager.hpp"
#include "Script/UI/KeyManager.hpp"
#include "Script/UI/MenuHandler.hpp"
#include "Script/GifManager.hpp"

namespace ClassPointers
{
	extern std::unique_ptr<DirectScript> g_DirectScript;
	extern std::unique_ptr<NativeScript> g_NativeScript;
	extern std::unique_ptr<HookManager> g_Hooker;
	extern std::unique_ptr<NativeInvoker> g_NativeInvoker;
	extern std::unique_ptr<ScriptThread> g_ScriptThread;
	extern std::unique_ptr<Logger> g_Logger;
	extern std::unique_ptr<Utility> g_Util;
	extern std::unique_ptr<TimerManager> g_Timers;
	extern std::unique_ptr<DirectThread> g_DirectThread;
	extern std::unique_ptr<YTDLoader> g_YtdLoader;
	extern std::unique_ptr<BuilderManager> g_NotificationManager;
	extern std::unique_ptr<VariableManager> g_VarManager;
    extern std::unique_ptr<WindowManager> g_WindowManager;
    extern std::unique_ptr<DrawManager> g_DrawManager;
    extern std::unique_ptr<MenuHandler> g_MenuManager;
    extern std::unique_ptr<KeyManager> g_KeyManager;
    extern std::unique_ptr<GifManager> g_GifManager;
}

#endif // CLASSPOINTERS_HPP
