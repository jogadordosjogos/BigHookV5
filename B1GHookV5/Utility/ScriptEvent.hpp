
#ifndef SCRIPTEVENT_HPP
#define SCRIPTEVENT_HPP
#include "pch.hpp"

template <std::size_t ArgCount>
class ScriptEvent
{
protected:
	std::array<std::uint64_t, ArgCount> m_Args{};
	std::uint32_t m_Bitset = 0;
public:
	void execute()
	{
		ClassPointers::g_ScriptThread->triggerScriptEvent()(1, m_Args, ArgCount, m_Bitset);
	}
};

class RemoteScriptEvent : public ScriptEvent<4>
{
public:
	RemoteScriptEvent(std::uint64_t eventId, std::uint32_t player);
};
#endif // SCRIPTEVENT_HPP
