#pragma once
#include "NotificationBuilder.hpp"

class ClanTagNotificationBuilder : public NotificationBuilder
{
public:
	virtual ~ClanTagNotificationBuilder() = default;;

	ClanTagNotificationBuilder* createNotification() override;

	ClanTagNotificationBuilder* addDescription(const std::string& desc) override;

	ClanTagNotificationBuilder* addType() override;

	ClanTagNotificationBuilder* addDict(const std::string& dict) override;

	ClanTagNotificationBuilder* addTex(const std::string& tex) override;

	ClanTagNotificationBuilder* addTitle(const std::string& title) override;

	ClanTagNotificationBuilder* addSubtitle(const std::string& sub) override;

	ClanTagNotificationBuilder* addClanTag(const std::string& clan) override;
};
