﻿#ifndef PCH_HPP
#define PCH_HPP
#define _CRT_SECURE_NO_WARNINGS
#include "targetver.h"

#pragma comment(lib, "ws2_32.lib")
#pragma comment(lib, "Winmm.lib")
// C++ Files

#include <windows.h>
#include <Mmsystem.h>
#include <string>
#include <vector>
#include <sstream>
#include <fstream>
#include <iostream>
#include <map>
#include <unordered_map>
#include <deque>
#include <stack>
#include <algorithm>
#include <functional>
#include <limits>
#include <Psapi.h>
#include <timeapi.h>
#include <ctime>
#include <thread>
#include <chrono>
#include <array>
#include <atomic>

//Lib files
#include <MinHook.h>


// Project Files
#include "Hooking//HookManager.hpp"
#include "Hooking//Hooks.hpp"
#include "Hooking//ScriptThread.hpp"
#include "Memory//SignatureScanner.hpp"
#include "Rage//Invoking//HashMap.hpp"
#include "Rage//Invoking//NativeInvoker.hpp"
#include "Rage//Invoking//Natives.hpp"
#include "Utility//ClassPointers.hpp"
#include "Utility//Log.hpp"
#include "Utility//types.hpp"
#include "Utility//enums.hpp"
#include "Utility//Util.hpp"
#include "Utility//Color.hpp"
#include "Utility//ScriptGlobal.hpp"
#include "Utility//ScriptEvent.hpp"
#include "Utility//ScriptPointer.hpp"
#include "Script//NativeScript.hpp"
#include "Script//DirectScript.hpp"



// Namespaces
using namespace std::string_literals;
using namespace std::chrono_literals;
#endif // PCH_HPP
