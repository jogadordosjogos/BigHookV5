#pragma once

#pragma pack(push, 1)
struct BigVec2
{
    float x;
    float y;

    BigVec2(const float xp, const float yp)
    {
        x = xp;
        y = yp;
    }
    BigVec2() = default;
};
#pragma pack(pop)

BigVec2 operator +(const BigVec2 a, const BigVec2 b);
BigVec2 operator -(const BigVec2 a, const BigVec2 b);
BigVec2 operator *(const BigVec2 a, const BigVec2 b);
BigVec2 operator /(const BigVec2 a, const BigVec2 b);

static_assert(sizeof(BigVec2) == sizeof(float) * 2, "BigVec2 size invalid");
