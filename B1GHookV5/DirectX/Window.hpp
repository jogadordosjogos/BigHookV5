#pragma once
class Window
{
    char m_Title[60] { };
    char m_Id[60] { };
    std::function<void()> m_OnTick;
    ImGuiWindowFlags m_Flags;
    ImVec2 m_Coords;
    ImVec2 m_Size;
    bool m_Rendering = true;
public:
    Window(const char *title, const char *id, ImGuiWindowFlags flags, ImVec2 coords, ImVec2 size,
           std::function<void()> &&lol, bool renderOnStart);
    ~Window();
    const char* getTitle() const { return m_Title; }
    const char* getIdentifier() const { return m_Id; }
    decltype(auto) getFunc() const { return m_OnTick; }
    decltype(auto) getCoords() const { return m_Coords; }
    decltype(auto) getSize() const { return m_Size; }
    decltype(auto) getRenderstate() const { return m_Rendering; }
    decltype(auto) getFlags () const { return m_Flags; }
};

