#include <pch.hpp>
#include "WindowManager.hpp"
#include "ImGUi/imgui_impl_dx11.h"
WindowManager::WindowManager()
{
}

WindowManager::~WindowManager()
{
}

void WindowManager::pushWindow(Window &&window)
{
    m_Multimap.insert(std::make_pair < const char*,Window* >(window.getIdentifier(), new Window(window)));
}

void WindowManager::callWindows()
{
    for(const auto& window : m_Multimap)
    {
        if (!window.second->getRenderstate())
            continue;
        ImGui::SetNextWindowPos(window.second->getCoords());
        ImGui::SetNextWindowSize(window.second->getSize());
        ImGui::Begin(window.second->getTitle(), nullptr, window.second->getFlags());
        if (window.second->getFunc())
            std::invoke(window.second->getFunc());
        ImGui::End();
        
    }
}