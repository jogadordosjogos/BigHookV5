#pragma once
#include "pch.hpp"
#include "ImGUi/imgui.h"
#include "ImGUi/imgui_internal.h"
#include "Window.hpp"

class WindowManager
{
    std::multimap<const char*, Window*> m_Multimap;
public:
    WindowManager();
    ~WindowManager();

    void pushWindow(Window&& window);
    void callWindows();

};
