#include "pch.hpp"
#include "Window.hpp"





Window::Window(const char *title, const char *id, const ImGuiWindowFlags flags, const ImVec2 coords, const ImVec2 size,
                std::function<void()> &&lol, const bool renderOnStart) : m_OnTick(lol), m_Flags(flags), m_Coords(coords), m_Size(size), m_Rendering(renderOnStart)
{
    std::strncpy(m_Title, title, sizeof(m_Title));
    std::strncpy(m_Id, id, sizeof(m_Id));
}

Window::~Window()
{
}
