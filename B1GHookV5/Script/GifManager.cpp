#include "pch.hpp"
#include "GifManager.hpp"

void GifManager::pushGif(const char* root, const int frames, const int start)
{
    m_Map[root] = MenuGif(root, frames, start, start);
    m_Roots.emplace_back(root);
    ClassPointers::g_Timers->addTimer(std::string(root) + " timer", 30ms, [this, root] {
        if (m_Map[root].currentframe + 1 > m_Map[root].framecount)
            m_Map[root].currentframe = m_Map[root].startframe;
        else
            m_Map[root].currentframe++; 
    });
}


void GifManager::updateFrames()
{

}
