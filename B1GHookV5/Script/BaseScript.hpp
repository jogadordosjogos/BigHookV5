
#ifndef BASESCRIPT_HPP
#define BASESCRIPT_HPP

class BaseScript
{
	bool m_Init = false;
public:
	virtual ~BaseScript() = default;
	virtual void initialize() = 0;
	virtual void onTick() = 0;
	virtual bool isInitialized() { return m_Init; }

	virtual void setInitState(const bool state) { m_Init = state; }
};
#endif // BASESCRIPT_HPP
