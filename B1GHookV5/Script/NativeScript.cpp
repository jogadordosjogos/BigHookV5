#include "pch.hpp"
#include "NativeScript.hpp"
#include "Utility/ClanTagNotificationBuilder.hpp"
#include "UI/Options/RegularOption.hpp"
#include "UI/Sub.hpp"
#include "UI/Options/NumberOption.hpp"
#include "UI/Options/SubOption.hpp"
#include "UI/Options/VectorOption.hpp"

void NativeScript::numeratePlayers()
{
    m_Players.clear();
    for (auto i = 0; i <= 32; ++i)
    {
        if (NETWORK::NETWORK_IS_PLAYER_CONNECTED(i))
        {
            m_Players.push_back(i);
        }
        else
        {
            m_Players.push_back(69);
        }
    }
}
bool g_GodMode = false;
void NativeScript::initialize()
{
    ClassPointers::g_NotificationManager->newBuilder<ClanTagNotificationBuilder>("WelcomeNotify")
        ->createNotification()
        ->addClanTag("B1G")
        ->addDict("commonmenu")
        ->addTex("shop_arrows_upanddown")
        ->addTitle("b1ghookV5")
        ->addSubtitle("has been hooked")
        ->addDescription("b1ghook besthook")
        ->addType()
        ->notify();
    ClassPointers::g_VarManager->addVar<std::vector<const char*>>({ "Self", "Ability Vector" }, { "HP", "Armor", "Ability (SP)" });
    ClassPointers::g_VarManager->addVar<int>({ "Self", "Ability Vector Pos" }, 0);
    ClassPointers::g_KeyManager->addKeyCheck("Close Open Menu", VK_INSERT);
    ClassPointers::g_KeyManager->addXblToKeyCheck("Close Open Menu", 0, ControlFrontendRb);
    ClassPointers::g_KeyManager->addKeyCheck("Menu Up", VK_NUMPAD8);
    ClassPointers::g_KeyManager->addXblToKeyCheck("Menu Up", 0, ControlFrontendUp);
    ClassPointers::g_KeyManager->addKeyCheck("Menu Down", VK_NUMPAD2);
    ClassPointers::g_KeyManager->addXblToKeyCheck("Menu Down", 0, ControlFrontendDown);
    ClassPointers::g_KeyManager->addKeyCheck("Menu Left", VK_NUMPAD4);
    ClassPointers::g_KeyManager->addXblToKeyCheck("Menu Left", 0, ControlFrontendLeft);
    ClassPointers::g_KeyManager->addKeyCheck("Menu Right", VK_NUMPAD6);
    ClassPointers::g_KeyManager->addXblToKeyCheck("Menu Right", 0, ControlFrontendRight);
    ClassPointers::g_KeyManager->addKeyCheck("Menu Select", VK_NUMPAD5);
    ClassPointers::g_KeyManager->addXblToKeyCheck("Menu Select", 0, ControlFrontendAccept);
    ClassPointers::g_KeyManager->addKeyCheck("Menu Back", VK_NUMPAD0);
    ClassPointers::g_KeyManager->addXblToKeyCheck("Menu Back", 0, ControlFrontendCancel);
}

bool g_TestBool = false;
std::int64_t g_TestInt = 5;
int g_VecInt = 5;
std::vector<const char*> epic = { "Firah", "Pocakking", "give-2" };
void NativeScript::onTick()
{
    static auto startTime = time(nullptr);
    srand(GetTickCount());
    initialize();
    ClassPointers::g_YtdLoader->init();
    ClassPointers::g_MenuManager->newSub(Sub("start menu", "Main Menu", [](Sub* sub) {
        sub->createOption(SubOption().addLeft("Network").addDesc("Modify the network.").addSub("network"));
        sub->createOption(RegularOption().addLeft("Regular Option").addDesc("Just a clickable option."));
        sub->createOption(NumberOption<int64_t>().addLeft("Number Option").addVar(g_TestInt).addMax(100).addMin(-100).addStep(5).addSuffix(" epic").addDesc("Just a clickable option."));
        sub->createOption(VectorOption().addLeft("Vector Option").addVar(g_VecInt).addVector(epic).addDesc("Scroll through a vector."));
		sub->createOption(RegularOption().addLeft("Unload").addDesc("Unloads the menu.").addFunction([]
		{
			ClassPointers::g_ScriptThread->shouldUninject() = true;
		}));
    }));
    ClassPointers::g_MenuManager->newSub(Sub("network", "Network", [](Sub* sub)
    {
        sub->createOption(SubOption().addLeft("Players").addDesc("Modify other players.").addSub("network.players"));
    }));
    ClassPointers::g_MenuManager->newSub(Sub("network.players", "Players", [this](Sub* sub)
    {
        for (auto player : m_Players)
        {
            if(player != 69)
            {
                std::ostringstream stream;
                stream << ClassPointers::g_ScriptThread->getPlayerName()(player);
                sub->createOption(SubOption().addLeft(stream.str().c_str()).addSub("network.playeroptions").addFunction([this, player] { m_CurrentPlayer = player; }));
            }
        }
    }, true));
    ClassPointers::g_MenuManager->newSub(Sub("network.playeroptions", ClassPointers::g_ScriptThread->getPlayerName()(m_CurrentPlayer), [this](Sub* sub)
    {
        sub->resetTitle(ClassPointers::g_ScriptThread->getPlayerName()(m_CurrentPlayer));
        sub->createOption(RegularOption().addLeft("Filler"));
    }, true));
    ClassPointers::g_MenuManager->checkKeys();

    while (true)
    {
        ClassPointers::g_DrawManager->drawText(ClassPointers::g_MenuManager->frameCount(), 0, { 106, 176, 76, 255 }, BigVec2(30, 20), BigVec2(0.25f, 0.25f), 1, BigVec2(0, 1920), true);

        ClassPointers::g_KeyManager->updateKeys();
        if(*ClassPointers::g_MenuManager->isOpen())
            ClassPointers::g_MenuManager->renderMenu();
        else
            ClassPointers::g_Timers->addTimer("OptionCheck Timer", 60ms, [] {
        });
       

        for (auto& func : ClassPointers::g_ScriptThread->functionVector())
        {
            func();
        }
        if(!ClassPointers::g_ScriptThread->functionVector().empty())
            ClassPointers::g_ScriptThread->clearFunctions();
        ClassPointers::g_DrawManager->clearNums();
        
        
        ClassPointers::g_Timers->updateTimers();
        ClassPointers::g_KeyManager->resetKeys();
        ClassPointers::g_GifManager->updateFrames();
        ClassPointers::g_ScriptThread->wait(0);
    }
    
}


