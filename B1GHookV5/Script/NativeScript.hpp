
#ifndef NATIVESCRIPT_HPP
#define NATIVESCRIPT_HPP
#include "BaseScript.hpp"

class CNetworkHandle
{
public:
    using sizeType = std::uint32_t;
    static constexpr std::uint32_t size = 0xD; // from scripps
    char data[size * sizeof(sizeType)];

    CNetworkHandle() = default;
    CNetworkHandle(int* const ptr)
    {
        std::memcpy(this, ptr, sizeof(*this));
    }

    operator int*()
    {
        return reinterpret_cast<int*>(this);
    }
};
class NativeScript : public BaseScript
{
    Player m_CurrentPlayer = 0;
    std::vector<Player> m_Players;
    void numeratePlayers();
public:

	NativeScript() = default;
	NativeScript(const NativeScript &obj) = default;
	NativeScript &operator=(const NativeScript &obj) = default;
	NativeScript(NativeScript &&obj) = default;
	NativeScript &operator=(NativeScript &&obj) = default;
	~NativeScript() = default;
	void initialize() override;
	void onTick() override;
};
#endif // NATIVESCRIPT_HPP
