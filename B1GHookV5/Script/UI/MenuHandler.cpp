#include "pch.hpp"
#include "MenuHandler.hpp"
#include "Utility/ClanTagNotificationBuilder.hpp"
#include "Utility/RegularNotificationBuilder.hpp"
void uppercaseKek(char* kek)
{
    for (; *kek; ++kek)
    {
        *kek = toupper(*kek);
    }
}

void MenuHandler::renderHeader()
{
	decltype(auto) currentSub = m_SubmenuStack.top();

    ClassPointers::g_DrawManager->drawRect({ 106, 176, 76, 255 }, m_MenuCoords - BigVec2(0, 8),
                                           BigVec2(m_MenuWidth, 100)); // Header Rectangle (or sprite)

	char gifFrameNameBuf[64] = {};
	//ClassPointers::g_GifManager->getFrame("ytd", gifFrameNameBuf);
    //ClassPointers::g_DrawManager->drawSprite("ytd", gifFrameNameBuf, m_MenuCoords - BigVec2(0, 8),
    //    BigVec2(m_MenuWidth, 100), { 255,255,255,255 }, 0);

    ClassPointers::g_DrawManager->drawText("bighookv5", 1, { 255, 255, 255, 255 },
                                           m_MenuCoords + BigVec2(0, m_MenuTitleTextOffsetY), BigVec2(1.f, 1.f), 0,
                                           BigVec2(0, 1920), false, true); // Header Text

    auto origName = currentSub->bar(); // Uppercased Submenu Name.
	char uppercasedName[40];
	std::memcpy(uppercasedName, origName, sizeof(uppercasedName));
	uppercaseKek(uppercasedName);

    ClassPointers::g_DrawManager->drawRect({ 0, 0, 0, 255 },
                                           BigVec2(m_MenuCoords.x, -1 * m_OptionTextYMultiplier + m_OptionRectOffsetY),
                                            BigVec2(m_MenuWidth, 40.f)); // Submenu Info Rect

    ClassPointers::g_DrawManager->drawText(uppercasedName, 0,
        { 106, 176, 76, 255 },
        BigVec2(m_MenuCoords.x - m_MenuWidth / m_TextWidthDivision, m_MenuCoords.y + 48),
        BigVec2(0.35f, 0.35f), 1), BigVec2(m_MenuCoords.x - m_MenuWidth / 2.085f, m_MenuCoords.x + m_MenuWidth / 2.085f); // Submenu Name

    std::snprintf(m_OptionCount, sizeof(m_OptionCount), "%zd / %zu", (*currentSub->currentOption()) + 1, (*currentSub->options()).size());

    ClassPointers::g_DrawManager->drawText(m_OptionCount, 0,
        { 106, 176, 76, 255 },
		{ m_MenuCoords.x + m_MenuWidth / m_TextWidthDivision, m_MenuCoords.y + 48 },
		{ 0.35f, 0.35f }, 2, { m_MenuCoords.x - m_MenuWidth / 2.085f, m_MenuCoords.x + m_MenuWidth / 2.085f }); // Selected Option / Count
}

void MenuHandler::renderFooter() const
{
	decltype(auto) currentSub = m_SubmenuStack.top();

    const auto footery = (*currentSub->options()).size() > m_MaxOptions
        ? m_MaxOptions * m_OptionTextYMultiplier + m_OptionRectOffsetY - 3.f
        : (*currentSub->options()).size() * m_OptionTextYMultiplier +
        m_OptionRectOffsetY - 3.f;
	// Footer Y Coord

	ClassPointers::g_DrawManager->drawRect(
		{ 0,0,0,225 },
		{ m_MenuCoords.x, footery },
		{ m_MenuWidth, 34.f });
	// Footer Rect

    if ((*currentSub->currentOption()) != 0) {
        ClassPointers::g_DrawManager->drawSprite("commonmenu",
            "arrowleft",
            BigVec2(m_MenuCoords.x, footery - 5.f),
            BigVec2(20, 20),
            { 106, 176, 76, 255 }, 90.f);
    } // Top Arrow

    if ((*currentSub->currentOption()) != (*currentSub->options()).size() - 1) {
        ClassPointers::g_DrawManager->drawSprite("commonmenu",
            "arrowright",
			{ m_MenuCoords.x, footery + 5.f },
			{ 20, 20 },
            { 106, 176, 76, 255 }, 90.f);
    } // Bottom Arrow

	if (*currentSub->currentOption() != 0 && currentSub->options() && (*currentSub->options())[*currentSub->currentOption()] && *((*currentSub->options())[*currentSub->currentOption()]->description()))
	{ // if desc isnt empty
		ClassPointers::g_DrawManager->drawRect(
			{ 106, 176, 76 ,225 },
			{ m_MenuCoords.x, footery + 20.f },
			{ m_MenuWidth, 3.f }); // Draw Border

		for (auto i = 0; i <= 3; i++)
			ClassPointers::g_DrawManager->drawSprite(
				"commonmenu", "header_gradient_script",
				{ m_MenuCoords.x, footery + 36.f },
				{ m_MenuWidth, 30.f },
				{ 255, 255, 255, 255 },
				0.f); // Gradient ting

        ClassPointers::g_DrawManager->drawText((*currentSub->options())[*currentSub->currentOption()]->description(), 0,
            { 255, 255, 255, 255 },
			{ m_MenuCoords.x - m_MenuWidth / m_TextWidthDivision, footery + 20.f },
			{ 0.3f, 0.3f }, 1,
			{ m_MenuCoords.x - m_MenuWidth / 2.085f, m_MenuCoords.x + m_MenuWidth / 2.085f }); // Description Texx
    }
}

void MenuHandler::renderOptions()
{
	decltype(auto) currentSub = m_SubmenuStack.top();
	decltype(auto) options = currentSub->options();
	auto currentOption = currentSub->currentOption();
	auto optionCount = options->size();

    std::size_t startPoint = 0; // Start
    std::size_t endPoint = options->size() > m_MaxOptions ? m_MaxOptions : optionCount; // Max;
    if (optionCount > m_MaxOptions && (*currentSub->currentOption()) >= optionCount) // if size is greater than the max
    {
        startPoint = *currentOption - options->size() + 1;
        endPoint = *currentOption + 1;
    }

    for (std::size_t i = startPoint, j = 0; i < endPoint; ++i, ++j)
    {
		decltype(auto) option = (*options)[i];
        if ((*currentSub->currentOption()) == i)
        {
            //ClassPointers::g_DrawManager->drawSprite("commonmenu", "gradient_nav", BigVec2(m_MenuCoords.x, j * m_OptionTextYMultiplier + m_OptionRectOffsetY),
            //   BigVec2(430.f, 40.f), { 255,255,255,255 }, 0.f); big alpha dont look gud
            ClassPointers::g_DrawManager->drawRect({ 255,255,255,255 }, BigVec2(m_MenuCoords.x, j * m_OptionTextYMultiplier + m_OptionRectOffsetY),
                BigVec2(m_MenuWidth, 40.f));
            ClassPointers::g_DrawManager->drawText(option->leftText(), 0,
                { 0, 0, 0, 255 },
                BigVec2(m_MenuCoords.x - m_MenuWidth / m_TextWidthDivision, j * m_OptionTextYMultiplier + m_OptionTextOffsetY),
                BigVec2(0.3f, 0.3f), 1, BigVec2(m_MenuCoords.x - m_MenuWidth / 2.085f, m_MenuCoords.x + m_MenuWidth / 2.085f));
            ClassPointers::g_DrawManager->drawText(option->rightText(), 0,
                { 0, 0, 0, 255 },
                BigVec2(m_MenuCoords.x + m_MenuWidth / m_TextWidthDivision - option->xOffset(), j * m_OptionTextYMultiplier + m_OptionTextOffsetY),
                BigVec2(0.3f, 0.3f), 2, BigVec2(m_MenuCoords.x - m_MenuWidth / 2.085f, m_MenuCoords.x + m_MenuWidth / m_TextWidthDivision - option->xOffset()));
            if (option->optType() == eOptionType::eNumber || option->optType() == eOptionType::eVector)
            {
                ClassPointers::g_DrawManager->drawSprite("commonmenu",
                    "shop_arrows_upanddown",
                    BigVec2(m_MenuCoords.x + m_MenuWidth / m_SpriteWidthDivision - 3.f,
                        j * m_OptionTextYMultiplier + m_OptionRectOffsetY + 1.f),
                    BigVec2(30, 32),
                    { 0,0,0,255 }, 90.f);
            }
            else if (option->useScrollerColor4Tex())
            {
                ClassPointers::g_DrawManager->drawSprite(option->texDict(),
                    option->tex(),
                    BigVec2(m_MenuCoords.x + m_MenuWidth / m_SpriteWidthDivision,
                        j * m_OptionTextYMultiplier + m_OptionRectOffsetY + 1.f),
                    BigVec2(20, 20),
                    { 0 , 0 , 0 ,255 }, 0.f);
            }
            else {
                ClassPointers::g_DrawManager->drawSprite(option->texDict(),
                    option->tex(),
                    BigVec2(m_MenuCoords.x + m_MenuWidth / m_SpriteWidthDivision,
                        j * m_OptionTextYMultiplier + m_OptionRectOffsetY + 1.f),
                    BigVec2(20, 20),
                    option->texColor(), 0.f);
            }
            
            ClassPointers::g_Timers->addTimer("OptionCheck Timer", 60ms, [&] {
				if (option->function())
				{
					if (ClassPointers::g_KeyManager->getKeyState("Menu Select"))
					{
						std::invoke(option->function());
					}
				}

				option->keyChecks();
            });

        }
        else
        {
            ClassPointers::g_DrawManager->drawRect({ 0,0,0,150 }, BigVec2(m_MenuCoords.x, j * m_OptionTextYMultiplier + m_OptionRectOffsetY),
                BigVec2(m_MenuWidth, 40.f));
            ClassPointers::g_DrawManager->drawText(option->leftText(), 0,
                { 255, 255, 255, 255 },
                BigVec2(m_MenuCoords.x - m_MenuWidth / m_TextWidthDivision, j * m_OptionTextYMultiplier + m_OptionTextOffsetY),
                BigVec2(0.3f, 0.3f), 1);
            ClassPointers::g_DrawManager->drawText(option->rightText(), 0,
                { 255, 255, 255, 255 },
                BigVec2(m_MenuCoords.x + m_MenuWidth / m_TextWidthDivision, j * m_OptionTextYMultiplier + m_OptionTextOffsetY),
                BigVec2(0.3f, 0.3f), 2, BigVec2(m_MenuCoords.x - m_MenuWidth / 2.085f, m_MenuCoords.x + m_MenuWidth / 2.085f));
            if (option->useScrollerColor4Tex())
            {
                ClassPointers::g_DrawManager->drawSprite(option->texDict(),
                    option->tex(),
                    BigVec2(m_MenuCoords.x + m_MenuWidth / m_SpriteWidthDivision,
                        j * m_OptionTextYMultiplier + m_OptionRectOffsetY + 1.f),
                    BigVec2(20, 20),
                    { 255 , 255 , 255 ,255 }, 0.f);
            }
            else {
                ClassPointers::g_DrawManager->drawSprite(option->texDict(),
                    option->tex(),
                    BigVec2(m_MenuCoords.x + m_MenuWidth / m_SpriteWidthDivision,
                        j * m_OptionTextYMultiplier + m_OptionRectOffsetY + 1.f),
                    BigVec2(20, 20),
                    option->texColor(), 0.f);
            }
        }


    }
}


void MenuHandler::renderMenu() {

    CONTROLS::DISABLE_CONTROL_ACTION(0, ControlPhone, true);
    renderHeader();
    renderOptions();
    renderFooter();

}

void MenuHandler::closeOpen(const bool open)
{
	m_MenuOpen = open;
}

const char* MenuHandler::frameCount()
{
    std::snprintf(m_FrameCount, sizeof(m_FrameCount), "%i FPS", static_cast<std::int32_t>(1.f / *ClassPointers::g_ScriptThread->frameTime()));
    return m_FrameCount;    
}

void MenuHandler::newSub(Sub&& submenu)
{
	auto newSub = new Sub(std::move(submenu));
	m_AllSubmenus.emplace_back(newSub);

	if (m_SubmenuStack.empty())
	{
		m_SubmenuStack.push(newSub);
	}
}

void MenuHandler::handleSwitch(const char* subName, const bool pushBack)
{
	for (decltype(auto) i : m_AllSubmenus)
	{
		if (std::strcmp(subName, i->identifier()) == 0)
		{
			m_SubmenuStack.push(i.get());
			break;
		}
	}
}

void MenuHandler::checkKeys()
{
    ClassPointers::g_Timers->addTimer("nav keycheck", 50ms, [this] {
        if (ClassPointers::g_KeyManager->getKeyState("Close Open Menu"))
		{
            ClassPointers::g_Timers->addTimer("OptionCheck Timer", 60ms, [] {});
            m_MenuOpen ^= true;
        }

        if (m_MenuOpen) {
			decltype(auto) currentSub = m_SubmenuStack.top();
			
			if (ClassPointers::g_KeyManager->getKeyState("Menu Up"))
            {
                if ((*currentSub->currentOption()) > 0)
                    --(*currentSub->currentOption());
                else
                    (*currentSub->currentOption()) = (*currentSub->options()).size() - 1;
            }
            if (ClassPointers::g_KeyManager->getKeyState("Menu Down"))
            {
                if ((*currentSub->currentOption()) < (*currentSub->options()).size() - 1)
					++(*currentSub->currentOption());
                else
                    (*currentSub->currentOption()) = 0;
            }
            if (ClassPointers::g_KeyManager->getKeyState("Menu Back"))
            {
				if (m_SubmenuStack.size() == 1)
				{
					ClassPointers::g_Timers->addTimer("OptionCheck Timer", 60ms, [] {
					});
					m_MenuOpen = false;
				}
				else
				{
					m_SubmenuStack.pop();
				}
            }
        }
    });
}
