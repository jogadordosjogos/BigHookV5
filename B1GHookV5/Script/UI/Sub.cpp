#include "pch.hpp"
#include "Sub.hpp"


Sub::Sub(const char *identifer, const char *barName, std::function<void(Sub*)>&& function, const bool dynamic)
	: m_Function(function),
    m_Dynamic(dynamic)
{
	std::memset(m_Bar, 0, sizeof(m_Bar));
    std::strncpy(m_Identifier, identifer, sizeof(m_Identifier));
    std::strncpy(m_Bar, barName, sizeof(m_Bar));
    if (m_Function)
        std::invoke(m_Function, this);
}

void Sub::resetTitle(const char *title)
{
    std::strncpy(m_Bar, title, sizeof(m_Bar));
}

void Sub::invoke()
{
	m_Options.clear();
    if (m_Function)
        std::invoke(m_Function, this);
}

const char* Sub::identifier() const
{
	return m_Identifier;
}

const char* Sub::bar() const
{
	return m_Bar;
}

void Sub::setDynamic(const bool dynamic)
{
    m_Dynamic = dynamic;
}

std::deque<std::shared_ptr<BaseOption>>* Sub::options()
{
	return &m_Options;
}

std::size_t* Sub::currentOption()
{
	return &m_CurrentOption;
}
