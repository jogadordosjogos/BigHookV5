#include "pch.hpp"
#include "KeyManager.hpp"

KeyManager::KeyManager()
{
}

KeyManager::~KeyManager()
{
}

void KeyManager::addKeyCheck(const char *accessName, const int key)
{
    m_Map[accessName] = { key, 0, 0, false };
    m_KeysToCheck.push_back(accessName);
}
void KeyManager::addXblToKeyCheck(const char *accessName, const int index, const int key)
{
    m_Map[accessName] = { m_Map[accessName].key, index, key, m_Map[accessName].down };
    m_KeysToCheck.push_back(accessName);
}
void KeyManager::changeKeyCheck(const char *accessName, int key)
{
    m_Map[accessName] = { key, false };
}

void KeyManager::updateKeys()
{
    for (auto& i : m_KeysToCheck)
    {
        if (GetAsyncKeyState(m_Map[i].key) & 0x8000) {
            m_PressedKeys.push_back(i);
            m_Map[i].down = true;
        }
        else if (m_Map[i].xblEquiv != 0 && CONTROLS::IS_CONTROL_PRESSED(m_Map[i].xblIndex, m_Map[i].xblEquiv)) {
            m_PressedKeys.push_back(i);
            m_Map[i].down = true;
        }
        else if (m_Map[i].xblEquiv != 0 && CONTROLS::IS_DISABLED_CONTROL_PRESSED(m_Map[i].xblIndex, m_Map[i].xblEquiv)) {
            m_PressedKeys.push_back(i);
            m_Map[i].down = true;
        }
        else
            m_Map[i].down = false;

    }
}
void KeyManager::resetKeys()
{
    for (auto& i : m_KeysToCheck)
    {

            m_Map[i].down = false;
    }
    m_PressedKeys.clear();
}
bool KeyManager::getKeyState(const char *accessName)
{
    return m_Map[accessName].down;
}

void KeyManager::removeKeyCheck(const char* accessName)
{
    for (size_t i = 0; i < m_KeysToCheck.size(); i++)
        if (m_KeysToCheck[i] == accessName)
            m_KeysToCheck.erase(m_KeysToCheck.begin() + i);
}
