#pragma once
struct KeyInfo
{
    int key;
    int xblIndex;
    int xblEquiv;
    bool down;
};
class KeyManager
{
    std::map<const char*, KeyInfo> m_Map;
    std::vector<const char*> m_PressedKeys;
    std::vector<const char*> m_KeysToCheck;
public:
    KeyManager();
    ~KeyManager();
    void addKeyCheck(const char* accessName, int key);
    void addXblToKeyCheck(const char *accessName, const int index, const int key);
    void changeKeyCheck(const char*accessName, int key);
    void updateKeys();
    void resetKeys();
    std::vector<const char*>& pressedKeys()
    {
        return m_PressedKeys;
    }

    bool getKeyState(const char* accessName);
    void removeKeyCheck(const char *accessName);
};
