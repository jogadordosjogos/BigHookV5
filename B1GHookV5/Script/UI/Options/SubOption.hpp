#pragma once

class SubOption : public BaseOption
{
private:
    char m_SubId[64];
public:
	SubOption()
		: BaseOption(eOptionType::eSub)
	{
		std::strncpy(m_TextureDict, "commonmenu", sizeof(m_TextureDict));
		std::strncpy(m_Texture, "arrowright", sizeof(m_Texture));
		m_TexColor = { 255, 255, 255, 255 };
		m_TextureColorScroller = true;
	}
    ~SubOption() override = default;

	bool keyChecks() override
	{
		if (ClassPointers::g_KeyManager->getKeyState("Menu Select"))
		{
			ClassPointers::g_MenuManager->handleSwitch(m_SubId);
			return false;
		}

		return true;
	}

	SubOption& addLeft(const char* text)
	{
		std::strncpy(m_LeftText, text, sizeof(m_LeftText));
		return *this;
	}

	SubOption& addDesc(const char* text)
	{
		std::strncpy(m_Descrtiption, text, sizeof(m_Descrtiption));
		return *this;
	}

    SubOption& addSub(const char* subId)
	{
		std::strncpy(m_SubId, subId, sizeof(m_SubId));
		return *this;
	}

    SubOption &addFunction(std::function<void()>&& func)
	{
		m_Function = std::move(func);
		return *this;
	}
};
