#pragma once
#include <iomanip>      // std::setprecision

template <typename NumberType>
class NumberOption : public BaseOption
{
	NumberType* m_Num = nullptr;
	NumberType m_Max = 10;
	NumberType m_Min = -10;
	NumberType m_Step = 1;
    
    std::string m_Suffix;
public:
    NumberOption()
		: BaseOption(eOptionType::eNumber)
    {
        m_XOffset = 26;
    }
    ~NumberOption() override = default;

    bool keyChecks() override
    {
        std::ostringstream stream;
        if (ClassPointers::g_KeyManager->getKeyState("Menu Right")) {
            
            if (*m_Num + m_Step > m_Max)
                *m_Num = m_Min;
            else
                *m_Num = *m_Num + m_Step;

            stream << std::setprecision(4) << *m_Num << m_Suffix;
            std::strncpy(m_RightText, stream.str().c_str(), sizeof(m_RightText));
        }
        if(ClassPointers::g_KeyManager->getKeyState("Menu Left"))
        {
            if (*m_Num - m_Step < m_Min)
                *m_Num = m_Max;
            else
                *m_Num = *m_Num - m_Step;

            stream << std::setprecision(4) << *m_Num << m_Suffix;
            std::strncpy(m_RightText, stream.str().c_str(), sizeof(m_RightText));
        }
        return true;
    };
    
    NumberOption& addLeft(const char* text)
    {
        std::strncpy(m_LeftText, text, sizeof m_LeftText);
        return *this;
    };

    NumberOption& addDesc(const char* text)
    {
        strncpy(m_Descrtiption, text, sizeof m_Descrtiption);
        return *this;
    };

    NumberOption& addSuffix(const char* suffix)
    {
        std::ostringstream stream;
        m_Suffix = suffix;
        stream << std::setprecision(4) << *m_Num << m_Suffix;
        std::strncpy(m_RightText, stream.str().c_str(), sizeof(m_RightText));
        return *this;
    }

    NumberOption& addMax(NumberType max)
    {
        m_Max = max;
        return *this;
    }

    NumberOption& addMin(NumberType min)
    {
        m_Min = min;
        return *this;
    }

    NumberOption& addStep(NumberType step)
    {
        m_Step = step;
        return *this;
    }

    NumberOption& addVar(NumberType& var)
    {
        std::ostringstream stream;
        m_Num = &var;
        stream.clear();
        stream << std::setprecision(4) << var;
        strncpy(m_RightText, stream.str().c_str(), sizeof(m_RightText));
        stream.clear();
        return *this;
    }

    NumberOption& addFunction(std::function<void()>&& func)
    {
        m_Function = std::move(func);
        return *this;
    }
};


