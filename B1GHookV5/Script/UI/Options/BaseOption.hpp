#pragma once
#include "../../Utility/Color.hpp"

enum class eOptionType
{
    eRegular,
    eBool,
    eVector,
    eNumber,
    eSub
};

class BaseOption
{
protected:
    char m_LeftText[64] = {};
    char m_RightText[64] = {};
    char m_Descrtiption[64] = {};
    char m_TextureDict[64] = {};
    char m_Texture[64] = {};
    float m_XOffset = 0.f;
    CColor4 m_TexColor = { 0, 0, 0, 0 };
    eOptionType m_Type = eOptionType::eRegular;
    std::function<void()> m_Function;
    bool m_TextureColorScroller = false;

	BaseOption() = default;
	BaseOption(eOptionType type)
		: m_Type(type)
	{}
public:
    virtual ~BaseOption() = default;
	virtual bool keyChecks()
	{
		return true;
	}

    auto const& function() const { return m_Function; }
    auto useScrollerColor4Tex() const { return m_TextureColorScroller; }
    auto leftText() const { return m_LeftText; }
    auto rightText() const { return m_RightText; }
    auto description() const { return m_Descrtiption; }
    auto texDict() const { return m_TextureDict; }
    auto tex() const { return m_Texture; }
    auto xOffset() const { return m_XOffset; }
    auto texColor() const { return m_TexColor; }
    auto optType() const { return m_Type; }
};
