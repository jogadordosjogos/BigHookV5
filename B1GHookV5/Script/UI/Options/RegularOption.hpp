#pragma once
#include "BaseOption.hpp"

class RegularOption : public BaseOption
{
public:
	RegularOption()
		: BaseOption(eOptionType::eRegular)
	{}
    ~RegularOption() override = default;

    RegularOption& addLeft(const char* text)
	{
		std::strncpy(m_LeftText, text, sizeof(m_LeftText));
		return *this;
	}

    RegularOption& addRight(const char* text)
	{
		std::strncpy(m_RightText, text, sizeof(m_RightText));
		return *this;
	}

    RegularOption& addDesc(const char* text)
	{
		std::strncpy(m_Descrtiption, text, sizeof(m_Descrtiption));
		return *this;
	}
	
	RegularOption& addDict(const char* text)
	{
		std::strncpy(m_TextureDict, text, sizeof(m_TextureDict));
		return *this;
	}

	RegularOption& addColor(CColor4 color)
	{
		m_TexColor = color;
		return *this;
	}

	RegularOption& addTexture(const char *text)
	{
		std::strncpy(m_Texture, text, sizeof(m_Texture));
		return *this;
	}

	RegularOption& addFunction(std::function<void()> func)
	{
		m_Function = func;
		return *this;
	}
};

 
