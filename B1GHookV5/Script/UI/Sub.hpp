#pragma once

class Sub
{
    char m_Identifier[40] = {};
    char m_Bar[40] = {};
    std::deque<std::shared_ptr<BaseOption>> m_Options;
    std::size_t m_CurrentOption = 0;
    std::function<void(Sub*)> m_Function;
    bool m_Dynamic = false;
public:
    Sub(const char *identifer, const char *barName, std::function<void(Sub*)>&& function, bool dynamic = false);
    ~Sub() = default;
    
    template <typename OptionType>
	void createOption(OptionType type);

    bool dynamic() const { return m_Dynamic; }
    void invoke();

	const char* identifier() const;
	const char* bar() const;

	void resetTitle(const char* title);
    void setDynamic(bool dynamic);

	std::deque<std::shared_ptr<BaseOption>>* options();
	std::size_t* currentOption();
};

template<typename OptionType>
inline void Sub::createOption(OptionType type)
{
	m_Options.emplace_back(new OptionType(type));
}
