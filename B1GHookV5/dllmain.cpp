﻿#include "pch.hpp"
BOOL APIENTRY DllMain(HMODULE const hModule, const DWORD reasonForCall, LPVOID const)
{
	if (reasonForCall == DLL_PROCESS_ATTACH)
	{
		DisableThreadLibraryCalls(hModule);
		CreateThread(nullptr, NULL, [](LPVOID hModule) -> DWORD
		{
			ClassPointers::g_Logger->initialize("BigHook");
			MH_Initialize();

			ClassPointers::g_ScriptThread->initialize();
			ClassPointers::g_DirectThread->initialize();

			ClassPointers::g_Logger->msg("Hooking Sigs.");
			ClassPointers::g_ScriptThread->hook();
			ClassPointers::g_DirectThread->hook();

			ClassPointers::g_Hooker->applyQueue();
			ClassPointers::g_Logger->msg("Hooked Sigs.");

			while (!(ClassPointers::g_ScriptThread->shouldUninject() || (GetForegroundWindow() == FindWindowA("grcWindow", nullptr) && (GetAsyncKeyState(VK_DELETE) & 0x8000))))
			{
				std::this_thread::yield();
			}

			ClassPointers::g_DirectThread->unhook();
			ClassPointers::g_Hooker->unhookAll();
			
			MH_Uninitialize();

			ClassPointers::g_Logger->uninitialize();
			FreeLibraryAndExitThread((HMODULE)hModule, EXIT_SUCCESS);
		}, hModule, NULL, nullptr);
	}

    return TRUE;
}
