#pragma once
#include "pch.hpp"

constexpr auto NUM_NATIVES = (5881);

extern std::uint64_t nativeHashes[NUM_NATIVES];
extern std::uint64_t scEntrypoints[NUM_NATIVES];
extern std::uint64_t steamEntrypoints[NUM_NATIVES];
