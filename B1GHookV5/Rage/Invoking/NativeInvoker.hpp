#pragma once
#include "pch.hpp"

#pragma pack(push, 1)
class NativeCallContext {
public:
	static constexpr std::size_t maxRetnCount = 0x03;
	static constexpr std::size_t maxArgCount = 0x64;
	using Value = std::uint64_t;

	static void(*vectorFixFunction)(NativeCallContext* dis);

	Value* retn = nullptr;
	std::uint64_t argCount = 0;
	Value* args = nullptr;
	std::uint32_t dataCount = 0;
	std::uint64_t reservedSpace[0x18] = {};

	DECLSPEC_NOINLINE void reset()
	{
		std::memset(retn, 0, sizeof(*retn) * maxRetnCount);
		std::memset(args, 0, sizeof(*args) * maxArgCount);
		argCount = 0;
		dataCount = 0;
		std::memset(reservedSpace, 0, sizeof(reservedSpace));
	}
};
#pragma pack(pop)

class NativeInvoker
{
public:
	using NativeHash = std::uint64_t;
	using EntrypointFunction = void(NativeCallContext* context);
private:
	std::unique_ptr<NativeCallContext::Value[]> m_ContextRetn = nullptr;
	std::unique_ptr<NativeCallContext::Value[]> m_ContextArgs = nullptr;
	std::unique_ptr<NativeCallContext> m_Context = nullptr;

	EntrypointFunction* getHandler(NativeHash hash);
public:
	void initialize();

	DECLSPEC_NOINLINE void beginCall();
	DECLSPEC_NOINLINE void endCall(NativeHash hash);

	template <typename T>
	DECLSPEC_NOINLINE void push(T value)
	{
		static_assert(sizeof(T) <= sizeof(NativeCallContext::Value));

		NativeCallContext::Value val = 0;
		*reinterpret_cast<T*>(&val) = value;
		m_Context->args[m_Context->argCount++] = val;
	}

	template <>
	DECLSPEC_NOINLINE void push<NativeVector3>(NativeVector3 value)
	{
		push(value.x);
		push(value.y);
		push(value.z);
	}

	template <typename T>
	FORCEINLINE T getReturnValue()
	{
		static_assert(sizeof(T) <= sizeof(NativeCallContext::Value) * NativeCallContext::maxRetnCount);
		return *reinterpret_cast<T*>(m_Context->retn);
	}

	template <>
	NativeVector3 getReturnValue()
	{
		EXCEPTION_POINTERS* expc = nullptr;
		__try
		{

			m_Context->vectorFixFunction(m_Context.get());
		}
		__except (expc = GetExceptionInformation(), EXCEPTION_EXECUTE_HANDLER)
		{
			// F's logger fucking sucks
		}

		return *reinterpret_cast<NativeVector3*>(m_Context->retn);
	}
};
