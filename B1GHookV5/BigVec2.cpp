#include "pch.hpp"
#include "BigVec2.hpp"

BigVec2 operator +(const BigVec2 a, const BigVec2 b)
{
    return BigVec2(a.x + b.x, a.y + b.y);
}

BigVec2 operator -(const BigVec2 a, const BigVec2 b)
{
    return BigVec2(a.x - b.x, a.y - b.y);
}
BigVec2 operator *(const BigVec2 a, const BigVec2 b)
{
    return BigVec2(a.x * b.x, a.y * b.y);
}
BigVec2 operator /(const BigVec2 a, const BigVec2 b)
{
    return BigVec2(a.x / b.x, a.y / b.y);
}
