
#ifndef HOOKS_HPP
#define HOOKS_HPP
#include "pch.hpp"
#include "..//Utility//types.hpp"
#include "Utility/ClassPointers.hpp"
#include <d3d10sdklayers.h>

void scriptThreadFunction(LPVOID /*lpParameter*/);
extern IDXGISwapChain* g_Swap;
void directThreadFunction( /*lpParameter*/);
extern bool g_InitDx;
namespace Hooks
{
	namespace Originals
	{
		extern tIsEntityUpsideDown g_OriginalIsEntityUpsideDown;
		extern HRESULT(*g_OriginalDirectXPresent)(IDXGISwapChain *pSwapChain, UINT syncInterval, UINT flags);
	}

	BOOL hkIsEntityUpsidedown( Entity entity);
	HRESULT hkDirect3DPresent(IDXGISwapChain *pSwapChain,  UINT syncInterval,  UINT flags);
}
#endif // HOOKS_HPP
