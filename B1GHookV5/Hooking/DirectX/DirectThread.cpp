#include <pch.hpp>
#include "DirectThread.hpp"

DirectThread::DirectThread(): m_Level()
{
}

DirectThread::~DirectThread()
= default;

void DirectThread::initialize()
{
    while (!m_Window)
    {
        m_Window = FindWindowA("grcWindow", nullptr);
        std::this_thread::yield();
    }
	m_Swapchain = "48 8B 0D ? ? ? ? 48 8D 55 A0 48 8B 01"_Scan.add(3).rip(4).as<decltype(m_Swapchain)&>();
	m_OriginalSwapchainVftable = *reinterpret_cast<SwapchainVftable**>(m_Swapchain);
	m_ReplacementSwapchainVftable = std::make_unique<SwapchainVftable>(*m_OriginalSwapchainVftable);
	m_ReplacementSwapchainVftable->Present = Hooks::hkDirect3DPresent;
	Hooks::Originals::g_OriginalDirectXPresent = m_OriginalSwapchainVftable->Present;

	m_Swapchain->GetDevice(__uuidof(m_Device), reinterpret_cast<void**>(&m_Device));
	m_Device->GetImmediateContext(&m_DeviceContext);

	ClassPointers::g_Logger->msg("Initialized DirectThread.");
}

void DirectThread::hook() 
{
	*reinterpret_cast<SwapchainVftable**>(m_Swapchain) = m_ReplacementSwapchainVftable.get();
}

void DirectThread::unhook() const
{
	*reinterpret_cast<SwapchainVftable**>(m_Swapchain) = m_OriginalSwapchainVftable;
}

void DirectThread::initializeTick()
{
    directThreadFunction();
}
