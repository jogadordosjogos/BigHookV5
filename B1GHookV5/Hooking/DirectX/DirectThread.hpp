#ifndef DIRECTTHREAD_HPP
#define DIRECTTHREAD_HPP
#include <DXGI.h>
#include <D3D11.h>

struct SwapchainVftable
{
	HRESULT(*QueryInterface)(IDXGISwapChain* This, REFIID riid, void** ppvObject);
	ULONG(*AddRef)(IDXGISwapChain* This);
	ULONG(*Release)(IDXGISwapChain* This);
	HRESULT(*SetPrivateData)(IDXGISwapChain* This, REFGUID Name, UINT DataSize, const void* pData);
	HRESULT(*SetPrivateDataInterface)(IDXGISwapChain* This, REFGUID Name, const IUnknown* pUnknown);
	HRESULT(*GetPrivateData)(IDXGISwapChain* This, REFGUID Name, UINT* pDataSize, void* pData);
	HRESULT(*GetParent)(IDXGISwapChain* This, REFIID riid, void** ppParent);
	HRESULT(*GetDevice)(IDXGISwapChain* This, REFIID riid, void** ppDevice);
	HRESULT(*Present)(IDXGISwapChain* This, UINT SyncInterval, UINT Flags);
	HRESULT(*GetBuffer)(IDXGISwapChain* This, UINT Buffer, REFIID riid, void** ppSurface);
	HRESULT(*SetFullscreenState)(IDXGISwapChain* This, BOOL Fullscreen, IDXGIOutput* pTarget);
	HRESULT(*GetFullscreenState)(IDXGISwapChain* This, BOOL* pFullscreen, IDXGIOutput** ppTarget);
	HRESULT(*GetDesc)(IDXGISwapChain* This, DXGI_SWAP_CHAIN_DESC *pDesc);
	HRESULT(*ResizeBuffers)(IDXGISwapChain* This, UINT BufferCount, UINT Width, UINT Height, DXGI_FORMAT NewFormat, UINT SwapChainFlags);
	HRESULT(*ResizeTarget)(IDXGISwapChain* This, const DXGI_MODE_DESC *pNewTargetParameters);
	HRESULT(*GetContainingOutput)(IDXGISwapChain* This, IDXGIOutput **ppOutput);
	HRESULT(*GetFrameStatistics)(IDXGISwapChain* This, DXGI_FRAME_STATISTICS *pStats);
	HRESULT(*GetLastPresentCount)(IDXGISwapChain* This, UINT *pLastPresentCount);
};

class DirectThread : public BaseThread
{
	D3D_FEATURE_LEVEL m_Level;
	IDXGISwapChain *m_Swapchain = nullptr;
	ID3D11Device *m_Device = nullptr;
	ID3D11DeviceContext *m_DeviceContext = nullptr;

	SwapchainVftable* m_OriginalSwapchainVftable = nullptr;
	std::unique_ptr<SwapchainVftable> m_ReplacementSwapchainVftable = nullptr;
    HWND m_Window = nullptr;
public:
	DirectThread();
	~DirectThread();

	/**
	 * \brief Initializes members for later hooking.
	 */
	void initialize() override;
	/**
	 * \brief Hooks m_SwapChainVtable for drawing.
	 */
	void hook() override;
	void unhook() const;

    void initializeTick() override;

	IDXGISwapChain* getSwapChain() const { return m_Swapchain; }
	ID3D11Device* getDevice() const { return m_Device; }
	ID3D11DeviceContext * getDeviceContext() const { return m_DeviceContext; }
    HWND window() const { return m_Window; }
};

#endif // DIRECTTHREAD_HPP
