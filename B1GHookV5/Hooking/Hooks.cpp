#pragma once
#include "pch.hpp"
#include "..//Utility//types.hpp"
#include "Utility/ClassPointers.hpp"
#include "Hooks.hpp"
#include <d3d10sdklayers.h>

void __stdcall scriptThreadFunction(LPVOID)
{
	try
	{
        ClassPointers::g_NativeScript->onTick();
        SwitchToFiber(ClassPointers::g_ScriptThread->mainFiber());

	}
	catch (const std::exception& e)
	{
        ClassPointers::g_Logger->msg("Failed hook %p with error %s", Hooks::Originals::g_OriginalIsEntityUpsideDown, e.what());
	}
}
IDXGISwapChain* g_Swap;
void __stdcall directThreadFunction()
{
	try
	{

        ClassPointers::g_DirectScript->onTick();
	}
	catch (const std::exception& e)
	{
		ClassPointers::g_Logger->fatal("Script Failed. Error: %s", e.what());
	}
}
bool g_InitDx = true;
namespace Hooks
{
	namespace Originals
	{
		tIsEntityUpsideDown g_OriginalIsEntityUpsideDown = nullptr;
		HRESULT(*g_OriginalDirectXPresent)(IDXGISwapChain *pSwapChain, UINT syncInterval, UINT flags);
	}

	BOOL hkIsEntityUpsidedown(Entity entity)
	{
		static uint64_t last = 0;
		const auto cur = *ClassPointers::g_ScriptThread->frameCount();
		if (last != cur)
		{
			last = cur;
			ClassPointers::g_ScriptThread->initializeTick();
		}
		return Originals::g_OriginalIsEntityUpsideDown(entity);
	}
	HRESULT hkDirect3DPresent(IDXGISwapChain *pSwapChain,  UINT syncInterval,  UINT flags)
	{
		if (g_InitDx)
		{
			g_Swap = pSwapChain;
		}
		ClassPointers::g_DirectThread->initializeTick();
		return Originals::g_OriginalDirectXPresent(pSwapChain, syncInterval, flags);
	}
}
