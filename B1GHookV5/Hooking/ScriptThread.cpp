#include "pch.hpp"

std::vector<void*> getEvents(char* b)
{
	std::vector<void*> events;
	auto i = 0, j = 0, matches = 0, found = 0;
	char *eventHookPattern = const_cast<char*>("\x4C\x8D\x05");
	while (found != 78)
	{
		if (b[i] == eventHookPattern[j])
		{
			if (++matches == 3)
			{
				events.push_back(
					reinterpret_cast <void*>(reinterpret_cast <uint64_t>(b + i - j) + *
						reinterpret_cast <int*>(
							b + i + 1) + 7));
				found++;
				j = matches = 0;
			}
			j++;
		}
		else
		{
			matches = j = 0;
		}
		i++;
	}
	return events;
}

void ScriptThread::initialize()
{
	m_IsSteam = static_cast<bool>(GetModuleHandleA("steam_api64.dll"));
	
	"74 48 E8 ? ? ? ? 48 8B 48 48"_Scan.add(3).rip(4).add(30).nop(2); // Model Requests
	ClassPointers::g_Logger->msg("Nop'd Model Requests Block.");
	//"48 8B C8 FF 52 30 84 C0 74 05 48"_Scan.add(8).nop(2); // Animal Models (b1g detecc)
	//ClassPointers::g_Logger->msg("Nop'd PlayerModel Block.");
	m_WorldPtr = "48 8B 05 ? ? ? ? 48 8B 58 08 48 8B C3 48 83 C4 20"_Scan.add(3).rip(4).as<decltype(m_WorldPtr)>(); // World Pointer.
	ClassPointers::g_Logger->msg("Scanned for World Pointer.");

	m_GlobalPtr = "48 83 EC 28 E8 ? ? ? ? 48 8B 0D ? ? ? ? 4C 8D 0D ? ? ? ? 4C 8D 05 ? ? ? ? BA 03"_Scan.add(3).rip(4).as<decltype(m_GlobalPtr)>(); // Global Ptr
	ClassPointers::g_Logger->msg("Scanned for Global Pointer.");

	m_EventPtr = getEvents("48 83 EC 28 E8 ? ? ? ? 48 8B 0D ? ? ? ? 4C 8D 0D ? ? ? ? 4C 8D 05 ? ? ? ? BA 03"_Scan.as<char*>());
	ClassPointers::g_Logger->msg("Scanned for Events. Count: %zu", m_EventPtr.size());

	m_IsEntityUpsideDown = "40 53 48 83 EC 20 33 DB 85 C9 78 10"_Scan.as<decltype(m_IsEntityUpsideDown)>(); // It's DOES_CAM_EXIST now but /shrug
	ClassPointers::g_Logger->msg("Scanned for Does Cam Exist.");

	m_TriggerScriptEvent = "48 8B C4 48 89 58 08 48 89 68 10 48 89 70 18 48 89 78 20 41 56 48 81 EC ? ? ? ? 45 8B F0 41 8B F9"_Scan.as<decltype(m_TriggerScriptEvent)>();
	ClassPointers::g_Logger->msg("Scanned for Trigger Script Event.");

	m_GetEventData = "48 89 5C 24 ? 57 48 83 EC 20 49 8B F8 4C 8D 05 ? ? ? ? 41 8B D9 E8 ? ? ? ? 48 85 C0 74 14 4C 8B 10 44 8B C3 48 8B D7 41 C1 E0 03 48 8B C8 41 FF 52 30 48 8B 5C 24 ? 48 83 C4 20 5F C3 48 89 5C 24"_Scan.as<decltype(m_GetEventData)>();
	ClassPointers::g_Logger->msg("Scanned for Get Event Data.");

	m_GetEventAtIndex = "40 53 48 83 EC 20 4C 8D 05 ? ? ? ? 83 CB FF"_Scan.as<decltype(m_GetEventAtIndex)>();
	ClassPointers::g_Logger->msg("Scanned for Get Event At Index.");

	m_GiveDelayedWeaponToPed = "48 89 5C 24 ? 48 89 6C 24 ? 48 89 74 24 ? 57 48 83 EC 30 41 8A E9"_Scan.as<decltype(m_GiveDelayedWeaponToPed)>();
	ClassPointers::g_Logger->msg("Scanned for Give Delayed Weapon To Ped.");

	m_HandleToPointer = "83 F9 FF 74 31 4C 8B 0D"_Scan.as<decltype(m_HandleToPointer)>();
	ClassPointers::g_Logger->msg("Scanned for Handle To Pointer.");

	m_GetLabelText = "40 53 48 83 EC 20 48 8B D9 48 8B D1 48 8D 0D ? ? ? ? E8 ? ? ? ? 84 C0 74 34"_Scan.as<decltype(m_GetLabelText)>();
	ClassPointers::g_Logger->msg("Scanned for Get Label Text.");

	m_PointerToHandle = "48 89 5C 24 ? 48 89 74 24 ? 57 48 83 EC 20 8B 15 ? ? ? ? 48 8B F9 48 83 C1 10 33 DB"_Scan.as<decltype(m_PointerToHandle)>();
	ClassPointers::g_Logger->msg("Scanned for Pointer To Handle.");
    
    m_DrawRect = "48 8B C4 48 89 58 08 57 48 83 EC 70 48 63 0D"_Scan.as<decltype(m_DrawRect)>();
    ClassPointers::g_Logger->msg("Scanned for Draw Rect.");

    m_BeginTextComponent = "48 83 EC 28 E8 ? ? ? ? E8 ? ? ? ? 33 C9 48 85 C0"_Scan.as<decltype(m_BeginTextComponent)>();
    ClassPointers::g_Logger->msg("Scanned for Begin Text.");

    m_DrawPoly = "48 83 EC 58 F3 0F 10 09 F3 0F 10 41"_Scan.as<decltype(m_DrawPoly)>();
    ClassPointers::g_Logger->msg("Scanned for Draw Poly.");

    m_EndTextComponent = "40 53 48 83 EC 40 0F 29 74 24 ? 0F 29 7C 24 ? 41 8B D8"_Scan.as<decltype(m_EndTextComponent)>();
    ClassPointers::g_Logger->msg("Scanned for End Text Component.");

    m_Email = "48 8D 0D ? ? ? ? 48 8D 50 10"_Scan.add(3).rip(4).as<char*>();
    ClassPointers::g_Logger->msg("Scanned for Email.");

    m_DrawSprite =
        "48 8B C4 48 89 58 08 48 89 68 10 48 89 70 18 57 48 81 EC ? ? ? ? 0F 29 70 E8 0F 29 78 D8 48 8B F2 48 8B D9 0F 28 FA 0F 28 F3 E8 ? ? ? ? 80 BC 24"_Scan
        .as < decltype(m_DrawSprite) >();
    ClassPointers::g_Logger->msg("Scanned for Draw Sprite.");

    m_AddTextComponenet = "48 89 5C 24 ? 48 89 6C 24 ? 48 89 74 24 ? 57 48 83 EC 20 8B 05 ? ? ? ? 4C 8B D1"_Scan.as<decltype(m_AddTextComponenet)>();
    ClassPointers::g_Logger->msg("Scanned for Add Text Componenet.");

    m_DrawLine = "41 8B CF C7 85 ? ? ? ? ? ? ? ? E8 ? ? ? ? B2 01"_Scan.add(-0xDC).as<decltype(m_DrawLine)>();
    ClassPointers::g_Logger->msg("Scanned for Draw Line.");

    m_HasTextureDictLoaded = "48 83 EC 28 4C 8B C1 48 8D 54 24 ? 48 8D 0D ? ? ? ? E8 ? ? ? ? 83 7C 24"_Scan.as<decltype(m_HasTextureDictLoaded)>();
    ClassPointers::g_Logger->msg("Scanned for Has Texture Dict Loaded.");

    m_RequestTextureDict = "40 53 48 83 EC 50 8A DA 4C 8B C1"_Scan.as<decltype(m_RequestTextureDict)>();
    ClassPointers::g_Logger->msg("Scanned for Request Texture Dict.");

	m_GetPlayerAddress = "40 53 48 83 EC 20 33 DB 38 1D ? ? ? ? 74 1C"_Scan.as<decltype(m_GetPlayerAddress)>();
	ClassPointers::g_Logger->msg("Scanned for Get Player Address.");

    m_GetPlayerName = "40 53 48 83 EC 20 80 3D ? ? ? ? ? 8B D9 74 22"_Scan.as<decltype(m_GetPlayerName)>();
    ClassPointers::g_Logger->msg("Scanned for Get Player Name.");

	m_GameState = "44 39 3D ? ? ? ? 75 08"_Scan.add(3).rip(4).as<decltype(m_GameState)>();
	ClassPointers::g_Logger->msg("Scanned for Game State.");

	m_FrameCount = "03 05 ? ? ? ? 89 81 ? ? ? ? E9"_Scan.add(2).rip(4).as<decltype(m_FrameCount)>();
	ClassPointers::g_Logger->msg("Scanned for Frame Count.");

	m_MaxWantedLevel = "8B 05 ? ? ? ? 89 41 6C 8B 05 ? ? ? ? 89 79 74"_Scan.add(2).rip(4).as<decltype(m_MaxWantedLevel)>();
	ClassPointers::g_Logger->msg("Scanned for Max Wanted Level.");

	m_TimeScale = "48 8D 05 ? ? ? ? 41 B9 ? ? ? ? F3 0F 10 00"_Scan.add(3).rip(4).as<decltype(m_TimeScale)>();
	ClassPointers::g_Logger->msg("Scanned for Time Scale.");

	m_IsSessionStarted = "38 1D ? ? ? ? 74 1C"_Scan.add(2).rip(4).as<decltype(m_IsSessionStarted)>();
	ClassPointers::g_Logger->msg("Scanned for Is Session Started.");

	m_Resolution = "66 0F 6E 05 ? ? ? ? F3 0F 10 6D"_Scan.add(4).rip(4).as<decltype(m_Resolution)>();
	ClassPointers::g_Logger->msg("Scanned for Game Resolution.");

	m_SnowPedTracks = "40 38 35 ? ? ? ? 43 8B 5C 19"_Scan.add(3).rip(4).as<decltype(m_SnowPedTracks)>();
	ClassPointers::g_Logger->msg("Scanned for Snow Ped Tracks.");

	m_SnowVehicleTracks = "8A 0D ? ? ? ? 41 8A 82"_Scan.add(2).rip(4).as<decltype(m_SnowVehicleTracks)>();
	ClassPointers::g_Logger->msg("Scanned for Snow Vehicle Tracks.");

	m_TvChannel = "89 0D ? ? ? ? 0F 85 ? ? ? ? 83 CD FF"_Scan.add(2).rip(4).as<decltype(m_TvChannel)>();
	ClassPointers::g_Logger->msg("Scanned for TV Channel.");

	m_TvVolume = "F3 0F 11 05 ? ? ? ? E8 ? ? ? ? 48 8B 0D ? ? ? ? 83 0D"_Scan.add(4).rip(4).as<decltype(m_TvVolume)>();
	ClassPointers::g_Logger->msg("Scanned for TV Volume.");

	m_DateTime = "89 0D ? ? ? ? 89 15 ? ? ? ? 44 89 05 ? ? ? ? F3 48 0F 2A C0 8B 05 ? ? ? ? 0F 5B C9"_Scan.add(2).rip(4).as<decltype(m_DateTime)>();
	ClassPointers::g_Logger->msg("Scanned for Date & Time.");

	m_NetworkTime = "8B 05 ? ? ? ? 89 05 ? ? ? ? 44 88 0D"_Scan.add(2).rip(4).as<decltype(m_NetworkTime)>();
	ClassPointers::g_Logger->msg("Scanned for Network Time.");

	m_TextInfo = "48 89 44 24 ? 48 8D 05 ? ? ? ? 48 89 44 24 ? 8B 05 ? ? ? ? 89 44 24 28"_Scan.add(8).rip(4).as<decltype(m_TextInfo)>();
	ClassPointers::g_Logger->msg("Scanned for Text Info.");

	m_HudColors = "45 8B 84 8E ? ? ? ? 48 8B CF"_Scan.add(4).rip(4).as<decltype(m_HudColors)>();
	ClassPointers::g_Logger->msg("Scanned for HUD Colors.");

	m_WaveIntensity = "F3 0F 59 1D ? ? ? ? F3 0F 59 D2 F3 0F 5C D9"_Scan.add(4).rip(4).as<decltype(m_WaveIntensity)>();
	ClassPointers::g_Logger->msg("Scanned for Wave Intensity.");

	m_FrameTime = "F3 0F 10 35 ? ? ? ? 48 8B CB FF 90 ? ? ? ? 8B D7"_Scan.add(4).rip(4).as<decltype(m_FrameTime)>();
	ClassPointers::g_Logger->msg("Scanned for Frame Time.");
	ClassPointers::g_Logger->msg("Waiting for game to load.");

	while (*m_GameState != eGameState::Playing)
	{
		std::this_thread::yield();
	}

	ClassPointers::g_NativeInvoker->initialize();
}

void ScriptThread::hook()
{
	ClassPointers::g_Hooker->hookDetour(reinterpret_cast<void*>(m_IsEntityUpsideDown),
        reinterpret_cast<void*>(&Hooks::hkIsEntityUpsidedown),
		reinterpret_cast<LPVOID*>(&Hooks::Originals::g_OriginalIsEntityUpsideDown), "Native Hook");
}

void ScriptThread::initializeTick()
{
    if (mainFiber() == nullptr)
    {
        setMainFiber(ConvertThreadToFiber(nullptr));

        if (mainFiber() == nullptr)
            setMainFiber(GetCurrentFiber());
    }

    if (timeGetTime() < m_WakeTime) return;

    if (secondFiber() != nullptr) {
        SwitchToFiber(secondFiber());
    }
    else {
        const auto fibre = CreateFiber(NULL, scriptThreadFunction, nullptr);
        setSecondFiber(fibre);
    }
}



void ScriptThread::wait(const int milli)
{
	m_WakeTime = timeGetTime() + milli;
	SwitchToFiber(mainFiber());
}
