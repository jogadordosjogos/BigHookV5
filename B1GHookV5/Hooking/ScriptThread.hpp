#ifndef SCRIPTTHREAD_HPP
#define SCRIPTTHREAD_HPP
#include "BaseThread.hpp"
#include "Utility/types.hpp"
#include "Rage/Invoking/NativeInvoker.hpp"

using tIsEntityUpsideDown     = BOOL(*)(std::int32_t entityHandle);
using tTriggerScriptEvent     = BOOL(*)(std::int32_t eventGroup, std::uint64_t* args, std::uint32_t argCount, std::uint32_t bitFlags);
using tGetEventData           = BOOL(*)(std::uint64_t eventGroup, std::uint64_t eventIndex, std::uint64_t* args, std::uint64_t argCount);
using tGetEventAtIndex        = std::uint64_t(*)(std::uint64_t eventGroup, std::uint64_t eventIndex);
using tHandleToPointer        = void*(*)(std::int32_t handle);
using tPointerToHandle	       = std::int32_t(*)(void* pointer);
using tGetPlayerAddress       = void*(*)(std::int32_t playerId);
using tGetPlayerName          = const char*(*)(std::int32_t playerId);
using tCreateVehicle          = std::int32_t(*)(std::uint32_t model, NativeVector3 pos, float heading, BOOL networked, BOOL unk);
using tGetLabelText           = const char*(*)(const char* label);
using tGiveDelayedWeaponToPed = void(*)(Ped ped, Hash weaponHash, std::int32_t ammoCount, BOOL equipNow);
using tHasTextureDictLoaded     = bool(*)(const char* dict);
using tRequestTextureDict       = void(*)(const char* dict);
using tDrawRect                 = void(*)(float x, float y, float width, float height, std::int32_t r, std::int32_t g, std::int32_t b, std::int32_t a);
using tDrawSprite               = void(*)(const char* textureDict, const char* textureName, float screenX, float screenY, float width, float height, float heading, std::int32_t red, std::int32_t green, std::int32_t blue, std::int32_t alpha);
using tDrawLine = void(*)(float x1, float y1, float z1, float x2, float y2, float z3, std::int32_t r, std::int32_t g, std::int32_t b, std::int32_t a);
using tDrawPoly = void(*)(float x1, float y1, float z1, float x2, float y2, float z2, float x3, float y3, float z3, std::int32_t red, std::int32_t green, std::int32_t blue, std::int32_t alpha);
using tAddTextComponent = void(*)(const char*);
using tEndTextComponent = void(*)(float x, float y);
using tBeginText = void(*)(const char*);

enum class eGameState : std::uint32_t
{
	Playing     = 0,
	Intro       = 1,
	LicenseShit = 3,
	MainMenu    = 5,
	LoadingSpMp = 6
};

class ScriptThread : public BaseThread
{
private:
	/**
	 * \brief Steam | Retail.
	 */
	bool m_IsSteam = false;

	/**
	 * \brief Current Game State.
	 */
	eGameState* m_GameState{};
	/**
	 * \brief World Pointer.
	 */
	CPedFactory** m_WorldPtr{};
	/**
	 * \brief Global Pointer.
	 */
	std::int64_t** m_GlobalPtr{};
	/**
	 * \brief Event Pointers.
	 */
	std::vector < void* > m_EventPtr{};
	/**
	 * \brief Frame Count.
	 */
	std::uint64_t* m_FrameCount{};
    
	/**
	 * \brief Hook Function.
	 */
	tIsEntityUpsideDown m_IsEntityUpsideDown{};

	/**
	* \brief Script Events.
	*/
	tTriggerScriptEvent m_TriggerScriptEvent{};

	/**
	* \brief Get Event Data.
	*/
	tGetEventData m_GetEventData{};

	/**
	* \brief Get Event At Index.
	*/
	tGetEventAtIndex m_GetEventAtIndex{};

	/**
	* \brief Get Label Text.
	*/
	tGetLabelText m_GetLabelText{};

	/**
	* \brief Converts script handles to pointers.
	*/
	tHandleToPointer m_HandleToPointer{};

	/**
	* \brief Converts pointers to script handles.
	*/
	tPointerToHandle m_PointerToHandle{};

	/**
	* \brief Gets a player's address from a Player ID.
	*/
	tGetPlayerAddress m_GetPlayerAddress{};

	/**
	* \brief Gets a player's name from a Player ID.
	*/
    tGetPlayerName m_GetPlayerName{};

	/**
	* \brief Gets a player's name from a Player ID.
	*/
	tGiveDelayedWeaponToPed m_GiveDelayedWeaponToPed{};

    /**
     * \brief Draws a line. Self explaintory.
     */
    tDrawLine m_DrawLine{};

    /**
    * \brief Draws a Rect. Self explaintory.
    */
    tDrawRect m_DrawRect{};

    /**
    * \brief Adds text. Self explaintory.
    */
    tAddTextComponent m_AddTextComponenet{};

    /**
    * \brief Checks if dict has loaded.
    */

    tHasTextureDictLoaded m_HasTextureDictLoaded{};
    /**
    * \brief Requests tex dict.
    */
    tRequestTextureDict m_RequestTextureDict{};

    /**
    * \brief Draws a sprite.
    */
    tDrawSprite m_DrawSprite{};

    /**
    * \brief Draws a polygon.
    */
    tDrawPoly m_DrawPoly{};

    /**
    * \brief Ends texx.
    */
    tEndTextComponent m_EndTextComponent{};

    /**
    * \brief Begins texx.
    */
    tBeginText m_BeginTextComponent{};

    char* m_Email;
	/**
	 * \brief Used for wait().
	 */
	DWORD m_WakeTime{ 0 };

	std::atomic_bool m_ShouldUninject = false;
public:
	ScriptThread(const ScriptThread &obj) = default;
	ScriptThread &operator=(const ScriptThread &obj) = default;
	ScriptThread(ScriptThread &&obj) = default;
	ScriptThread &operator=(ScriptThread &&obj) = default;

	ScriptThread() = default;
	~ScriptThread() = default;
	/**
	 * \brief Finds Patterns, does needed things for script to work.
	 */
	void initialize() override;
	/**
	 * \brief Hooks natives/sigs.
	 */
	void hook() override;
    void initializeTick() override;
    /**
	 * \brief Initialzes/Switches fibers, and starts native script tick.
	 */

	/**
	 * \brief Delays the thread/script.
	 * \param milli Millisecond delay.
	 */
	void wait(int milli);

	// cba to document getters
    decltype(auto) frameTime() const { return m_FrameTime; }
    decltype(auto) email() const { return m_Email; }
    decltype(auto) beginText() const { return m_BeginTextComponent; }
    decltype(auto) endTextComponent() const { return m_EndTextComponent; }
    decltype(auto) addTextComponent() const { return m_AddTextComponenet; }
    decltype(auto) drawRect() const { return m_DrawRect; }
    decltype(auto) drawLine() const { return m_DrawLine; }
    decltype(auto) requestDict() const { return m_RequestTextureDict; }
    decltype(auto) hasDictLoaded() const { return m_HasTextureDictLoaded; }
    decltype(auto) drawPoly() const { return m_DrawPoly; }
	decltype(auto) gameState() const { return m_GameState; }
    CPed* localPlayer() const
    {
        if (const auto pedFactory = *m_WorldPtr)
            return pedFactory->localPlayer;
        return nullptr;
    }
	decltype(auto) worldPtr() const { return m_WorldPtr; }
	decltype(auto) globalPtr() const { return m_GlobalPtr; }
	decltype(auto) eventPtr() const { return m_EventPtr; }
	decltype(auto) frameCount() const { return m_FrameCount; }
	decltype(auto) isEntUpsideDown() const { return m_IsEntityUpsideDown; }
    decltype(auto) drawSprite() const { return m_DrawSprite; }
	decltype(auto) triggerScriptEvent() const { return m_TriggerScriptEvent; }
    decltype(auto) getPlayerName() const { return m_GetPlayerName; }
	decltype(auto) handleToPointer() const { return m_HandleToPointer; }
	decltype(auto) pointerToHandle() const { return m_PointerToHandle; }
	decltype(auto) getPlayerAddress() const { return m_GetPlayerAddress; }
    decltype(auto) maxWantedLvl()const { return m_MaxWantedLevel; }
    decltype(auto) timeScale()const { return m_TimeScale; }
    decltype(auto) isSessionStarted()const { return m_IsSessionStarted; }
    decltype(auto) gameRes()const { return m_Resolution; }
    decltype(auto) snowPedTracks()const { return m_SnowPedTracks; }
    decltype(auto) snowVehicleTracks()const { return m_SnowVehicleTracks; }
    decltype(auto) currentChannel()const { return m_TvChannel; }
    decltype(auto) currentTeleVol()const { return m_TvVolume; }
    decltype(auto) dateTime()const { return m_DateTime; }
    decltype(auto) networkTime()const { return m_NetworkTime; }
    decltype(auto) texxInfo()const { return m_TextInfo; }
    decltype(auto) hudColors()const { return m_HudColors; }
    decltype(auto) waveIntensity()const { return m_WaveIntensity; }
    decltype(auto) gibDelayedWep()const { return m_GiveDelayedWeaponToPed; }
    decltype(auto) getPlayerPed(std::uint32_t player) const { return m_PointerToHandle(m_GetPlayerAddress(player)); }
	decltype(auto) isSteam() const { return m_IsSteam; }
	auto& shouldUninject() { return m_ShouldUninject; }

private:

	/**
	* \brief Local Player's Max Wanted Level.
	*/
	std::int32_t* m_MaxWantedLevel{};

	/**
	* \brief Local Game Speed.
	*/
	float* m_TimeScale{};

	/**
	* \brief Is Session Started.
	*/
	bool* m_IsSessionStarted{};

	/**
	* \brief Game Resolution.
	*/
	CResolution* m_Resolution{};

	/**
	* \brief Ped Tracks in Snow.
	*/
	bool* m_SnowPedTracks{};

	/**
	* \brief Vehicle Tracks in Snow.
	*/
	bool* m_SnowVehicleTracks{};

	/**
	* \brief TV Channel.
	*/
	std::int32_t* m_TvChannel{};

	/**
	* \brief TV Volume.
	*/
	float* m_TvVolume{};

	/**
	* \brief Date & Time.
	*/
	CDateTime* m_DateTime{};

	/**
	* \brief Network Time.
	*/
	std::uint64_t* m_NetworkTime{};

	/**
	* \brief Text Info.
	*/
	CTextInfo* m_TextInfo{};

	/**
	* \brief HUD Colors.
	*/
	CHudColor* m_HudColors{};

	/**
	* \brief Wave Intensity.
	*/
	float* m_WaveIntensity{};

	/**
	* \brief Frame Time.
	*/
	float* m_FrameTime{};
};

#endif // SCRIPTTHREAD_HPP
