#include "pch.hpp"
#include "Utility/ClassPointers.hpp"

HookManager::HookManager()
= default;

HookManager::~HookManager()
= default;

eHookResult HookManager::hookDetour(void *function, void *detour, void **original, const char* name)
{
    auto retVal = eHookResult::eHookFailed;
    ClassPointers::g_Logger->msg("Hooking %s", name);

    if (MH_CreateHook(function, detour, original) == MH_OK)
    {
        MH_QueueEnableHook(function);
        ClassPointers::g_Logger->msg("Queued hook %s", name);
        retVal = eHookResult::eHookOk;
        m_Hooks.push_back(function);
    }
    else
    {
        ClassPointers::g_Logger->error("Failed to create hook %s", name);
    }

    return retVal;
}

void HookManager::applyQueue()
{
    MH_ApplyQueued();
    for (auto hook : m_Hooks)
	{
        ClassPointers::g_Logger->msg("Enabled hook %p", hook);
    }
}

void HookManager::unhookAll()
{
	ClassPointers::g_Logger->msg("Unhooking.");

	for (auto a : m_Hooks)
	{
		MH_DisableHook(a);
		MH_RemoveHook(a);
	}

	ClassPointers::g_DirectThread->unhook();
}


